import sys
import pandas as pd
import numpy as np
from surprise import SVD
from surprise import Dataset
from surprise import Reader
import pickle
from pathlib import Path

similarIds = sys.argv[2].split(',')
customerId = sys.argv[3]
recommendations = []

fileName = sys.argv[1].removesuffix('.csv')
my_file = Path(fileName)
if my_file.is_file():
    with open(fileName, 'rb') as f:
        algo = pickle.load(f)
        for simId in similarIds:
            recommendations = np.append(recommendations, algo.predict(int(customerId), int(simId)).est)
        recommendations = np.argsort(recommendations)[::-1]
        print('[', end='')
        for r in recommendations:
            print(r, end=',')
        print(']', end='')

else:
    rg = pd.read_csv(sys.argv[1])
    reader = Reader(rating_scale=(0, 1))
    data = Dataset.load_from_df(rg[['customerId', 'itemId', 'rating']], reader)

    trainset = data.build_full_trainset()

    algo = SVD()
    algo.fit(trainset)

    with open(fileName, 'wb') as f:
        pickle.dump(algo, f)

    for simId in similarIds:
        recommendations.append(algo.predict(int(customerId), int(simId)).est)

    recommendations = np.argsort(np.array(recommendations))[::-1]

    print('[', end='')
    for r in recommendations:
        print(r, end=',')
    print(']', end='')
