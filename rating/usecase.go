package rating

import (
	"context"
	"diploma/entities"
	"diploma/types"
)

type Usecase interface {
	Add(ctx context.Context, userId int, modelName string, contentId string, customerId string,
		time types.JsonNullTime, rating types.JsonNullFloat) (entities.Rating, error)
	GetByContentId(ctx context.Context, contentId string, modelName string, userId int) ([]entities.Rating, error)
	GetByCustomerId(ctx context.Context, customerId string, userId int) ([]entities.Rating, error)
	Delete(ctx context.Context, customerId string, contentId string, timeAt types.JsonNullTime, modelName string,
		userId int) error
	DeleteByCustomerId(ctx context.Context, customerId string, userId int) error
	DeleteByItemId(ctx context.Context, content string, modelName string, userId int) error
}
