package usecase

import (
	"context"
	"diploma/content"
	"diploma/contentModel"
	"diploma/customer"
	"diploma/entities"
	"diploma/rating"
	"diploma/types"
)

type RatingUsecase struct {
	ratingRepository   rating.Repository
	contentRepository  content.Repository
	customerRepository customer.Repository
	modelRepository    contentModel.Repository
}

func NewRatingUsecase(ratingRepo rating.Repository, contentRepo content.Repository, customerRepo customer.Repository,
	modelRepo contentModel.Repository) *RatingUsecase {
	return &RatingUsecase{
		ratingRepository:   ratingRepo,
		contentRepository:  contentRepo,
		customerRepository: customerRepo,
		modelRepository:    modelRepo,
	}
}

func (u RatingUsecase) Add(ctx context.Context, userId int, modelName string, contentId string, customerId string,
	time types.JsonNullTime, rating types.JsonNullFloat) (entities.Rating, error) {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return entities.Rating{}, err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return entities.Rating{}, err
	}

	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		if customerLocalId == -1 {
			_, _ = u.customerRepository.Add(ctx, customerId, userId)
		}
		return entities.Rating{}, err
	}

	err = u.ratingRepository.Add(ctx, itemId, customerLocalId, time, rating)
	if err != nil {
		return entities.Rating{}, err
	}

	return u.ratingRepository.Get(ctx, itemId, customerLocalId, userId)
}

func (u RatingUsecase) GetByContentId(ctx context.Context, contentId string, modelName string,
	userId int) ([]entities.Rating, error) {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return nil, err
	}

	return u.ratingRepository.FindByContentId(ctx, itemId, userId)
}

func (u RatingUsecase) GetByCustomerId(ctx context.Context, customerId string, userId int) ([]entities.Rating, error) {
	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return nil, err
	}

	return u.ratingRepository.FindByCustomerId(ctx, customerLocalId, userId)
}

func (u RatingUsecase) Delete(ctx context.Context, customerId string, contentId string, timeAt types.JsonNullTime,
	modelName string, userId int) error {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return err
	}

	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return err
	}

	return u.ratingRepository.Delete(ctx, itemId, customerLocalId, timeAt)
}

func (u RatingUsecase) DeleteByCustomerId(ctx context.Context, customerId string, userId int) error {
	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return err
	}

	return u.ratingRepository.DeleteByCustomerId(ctx, customerLocalId)
}

func (u RatingUsecase) DeleteByItemId(ctx context.Context, content string, modelName string, userId int) error {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, content, modelId)
	if err != nil {
		return err
	}

	return u.ratingRepository.DeleteByItemId(ctx, itemId)
}
