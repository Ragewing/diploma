package postgre

import (
	"context"
	"database/sql"
	"diploma/entities"
	"diploma/types"
	"time"
)

type RatingRepository struct {
	db *sql.Conn
}

func NewRatingRepository(db *sql.Conn) *RatingRepository {
	return &RatingRepository{db: db}
}

func (r *RatingRepository) Get(ctx context.Context, itemId int, customerId, userId int) (entities.Rating, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT r.*
			   FROM ratings r,
				    items i,
				    content_models cm
			   WHERE r.customer_id = $1
			     AND r.content_id = $2
			     AND i.item_id = r.content_id
			     AND i.model_id = cm.model_id
			     AND cm.user_id = $3
			   ORDER BY time_at DESC`,
		customerId, itemId, userId)

	defer rows.Close()

	if err != nil {
		return entities.Rating{}, err
	}

	var rating entities.Rating
	for rows.Next() {
		err := rows.Scan(&rating.ContentId, &rating.CustomerId, &rating.Rating, &rating.TimeAt)
		if err != nil {
			return entities.Rating{}, err
		}
	}

	return rating, err
}

func (r *RatingRepository) FindByContentId(ctx context.Context, itemId, userId int) ([]entities.Rating, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT r.*
			   FROM ratings r,
			        items i,
				    content_models cm
			   WHERE r.content_id = $1
			     AND i.item_id = r.content_id
			     AND i.model_id = cm.model_id
			     AND cm.user_id = $2
			   ORDER BY time_at DESC`,
		itemId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var ratings []entities.Rating
	for rows.Next() {
		var rating entities.Rating
		err := rows.Scan(&rating.ContentId, &rating.CustomerId, &rating.Rating, &rating.TimeAt)
		if err != nil {
			return nil, err
		}

		ratings = append(ratings, rating)
	}

	return ratings, err
}

func (r *RatingRepository) FindByCustomerId(ctx context.Context, customerId, userId int) ([]entities.Rating, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT *
			   FROM ratings r,
			        items i,
				    content_models cm
			   WHERE r.customer_id = $1
			     AND i.item_id = r.content_id
			     AND i.model_id = cm.model_id
			     AND cm.user_id = $2
			   ORDER BY time_at DESC`,
		customerId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var ratings []entities.Rating
	for rows.Next() {
		var rating entities.Rating
		err := rows.Scan(&rating.ContentId, &rating.CustomerId, &rating.Rating, &rating.TimeAt)
		if err != nil {
			return nil, err
		}

		ratings = append(ratings, rating)
	}

	return ratings, err
}

func (r *RatingRepository) Add(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime,
	rating types.JsonNullFloat) error {
	if !timeAt.Valid {
		timeAt.Time = time.Now()
		timeAt.Valid = true
	}

	_, err := r.db.ExecContext(ctx,
		`INSERT INTO ratings (content_id, customer_id, time_at, rating)
			   VALUES ($1, $2, $3, $4)`,
		itemId, customerId, timeAt, rating)

	if err != nil {
		return err
	}

	return nil
}

func (r *RatingRepository) Delete(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime) error {
	id := -1
	if !timeAt.Valid {
		err := r.db.QueryRowContext(ctx,
			`DELETE
				   FROM ratings r
				   WHERE r.customer_id = $1
					 AND r.content_id = $2 
				   RETURNING r.content_id`,
			customerId, itemId).Scan(&id)

		return err
	} else {
		_, err := r.db.ExecContext(ctx,
			`DELETE
				   FROM ratings r
				   WHERE r.customer_id = $1
					 AND r.content_id = $2
					 AND r.time_at = $3`,
			customerId, itemId, timeAt)

		return err
	}
}

func (r *RatingRepository) DeleteByCustomerId(ctx context.Context, customerId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE
			   FROM ratings r
			   WHERE r.customer_id = $1`,
		customerId)

	return err
}

func (r *RatingRepository) DeleteByItemId(ctx context.Context, itemId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE
			   FROM ratings r
			   WHERE r.content_id = $1`,
		itemId)

	return err
}

func (r *RatingRepository) FindByModelId(ctx context.Context, modelId, userId int) ([]entities.Rating, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT iv.value, r.customer_id, r.rating, r.time_at
			   FROM ratings r,
                    items i,
                    items_values iv,
                    content_models cm
               WHERE i.item_id = r.content_id
                 AND i.model_id = $1
                 AND cm.model_id = i.model_id
                 AND cm.user_id = $2
                 AND iv.item_id = i.item_id
                 AND iv.attribute_id = 1
               ORDER BY r.customer_id`,
		modelId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var ratings []entities.Rating
	for rows.Next() {
		var rating entities.Rating
		err := rows.Scan(&rating.ContentId, &rating.CustomerId, &rating.Rating, &rating.TimeAt)
		if err != nil {
			return nil, err
		}

		ratings = append(ratings, rating)
	}

	return ratings, err
}

func (r *RatingRepository) GetCustomersCount(ctx context.Context, modelId, userId int) (int, error) {
	count := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT COUNT(DISTINCT r.customer_id)
			   FROM ratings r,
				    items i,
				    content_models cm
			   WHERE i.item_id = r.content_id
			     AND i.model_id = $1
			     AND cm.model_id = i.model_id
			     AND cm.user_id = $2`,
		modelId, userId).Scan(&count)

	return count, err
}

func (r *RatingRepository) GetItemsCount(ctx context.Context, modelId, userId int) (int, error) {
	count := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT COUNT(DISTINCT r.content_id)
			   FROM ratings r,
				    items i,
				    content_models cm
			   WHERE i.item_id = r.content_id
			     AND i.model_id = $1
			     AND cm.model_id = i.model_id
			     AND cm.user_id = $2`,
		modelId, userId).Scan(&count)

	return count, err
}
