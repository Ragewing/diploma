package rating

import (
	"context"
	"diploma/entities"
	"diploma/types"
)

type Repository interface {
	Get(ctx context.Context, itemId int, customerId int, userId int) (entities.Rating, error)
	FindByContentId(ctx context.Context, itemId, userId int) ([]entities.Rating, error)
	FindByCustomerId(ctx context.Context, customerId int, userId int) ([]entities.Rating, error)
	Add(ctx context.Context, itemId int, customerId int, time types.JsonNullTime, rating types.JsonNullFloat) error
	Delete(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime) error
	DeleteByCustomerId(ctx context.Context, customerId int) error
	DeleteByItemId(ctx context.Context, itemId int) error
	FindByModelId(ctx context.Context, modelId, userId int) ([]entities.Rating, error)
	GetCustomersCount(ctx context.Context, modelId, userId int) (int, error)
	GetItemsCount(ctx context.Context, modelId, userId int) (int, error)
}
