package http

import (
	"diploma/rating"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, ratingUC rating.Usecase, userUC user.Usecase) {
	h := NewHandler(ratingUC, userUC)

	views := router.Group("/ratings")
	{
		views.POST("", h.Add)
		views.GET("/byContentId", h.GetRatingsByContentId)
		views.GET("/byCustomerId", h.GetRatingsByCustomerId)
		views.DELETE("", h.DeleteRatings)
	}
}
