package http

import (
	"diploma/contentModel"
	"diploma/entities"
	"diploma/rating"
	"diploma/types"
	"diploma/user"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	useCase rating.Usecase
	userUC  user.Usecase
}

func NewHandler(usecase rating.Usecase, userUC user.Usecase) *Handler {
	return &Handler{
		useCase: usecase,
		userUC:  userUC,
	}
}

type addRatingInput struct {
	ModelName  string              `json:"model_name" binding:"required"`
	UserKey    string              `json:"user_key" binding:"required"`
	ContentId  string              `json:"content_id" binding:"required"`
	CustomerId string              `json:"customer_id" binding:"required"`
	TimeAt     types.JsonNullTime  `json:"time_at"`
	Rating     types.JsonNullFloat `json:"rating" binding:"required"`
}

type addRatingResponse struct {
	Rating entities.Rating `json:"view"`
	Error  string          `json:"error"`
}

func (h *Handler) Add(c *gin.Context) {
	var inp addRatingInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	addedRating, err := h.useCase.Add(ctx, foundUser.Id, inp.ModelName, inp.ContentId, inp.CustomerId,
		inp.TimeAt, inp.Rating)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &addRatingResponse{
		Rating: addedRating,
		Error:  errMsg,
	})
}

type getRatingsResponse struct {
	Rating []entities.Rating `json:"views"`
	Error  string            `json:"error"`
}

func (h *Handler) GetRatingsByContentId(c *gin.Context) {
	contentId := c.Query("contentId")
	modelId := c.Query("modelName")
	key := c.Query("key")

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	ratings, err := h.useCase.GetByContentId(ctx, contentId, modelId, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getRatingsResponse{
		Rating: ratings,
		Error:  errMsg,
	})
}

func (h *Handler) GetRatingsByCustomerId(c *gin.Context) {
	customerId := c.Query("customerId")
	key := c.Query("key")

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	ratings, err := h.useCase.GetByCustomerId(c.Request.Context(), customerId, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getRatingsResponse{
		Rating: ratings,
		Error:  errMsg,
	})
}

type deleteRatingsInput struct {
	UserId     int                `json:"user_id" binding:"required"`
	ModelName  string             `json:"model_name" binding:"required"`
	CustomerId string             `json:"customer_id" binding:"required"`
	ContentId  string             `json:"content_id" binding:"required"`
	TimeAt     types.JsonNullTime `json:"time_at"`
}

type deleteRatingsResponse struct {
	Error string `json:"error"`
}

func (h *Handler) DeleteRatings(c *gin.Context) {
	var inp deleteRatingsInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := h.useCase.Delete(c.Request.Context(), inp.CustomerId, inp.ContentId, inp.TimeAt, inp.ModelName, inp.UserId)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &deleteRatingsResponse{
		Error: errMsg,
	})
}
