package postgre

import (
	"context"
	"database/sql"
	"diploma/entities"
	"diploma/types"
	"time"
)

type PurchaseRepository struct {
	db *sql.Conn
}

func NewPurchaseRepository(db *sql.Conn) *PurchaseRepository {
	return &PurchaseRepository{db: db}
}

func (r *PurchaseRepository) Get(ctx context.Context, itemId int, customerId int, userId int) (entities.Purchase, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT *
			   FROM purchases p
		       WHERE p.customer_id = 1
			     AND p.content_id = 1
			   ORDER BY time_at DESC
			   LIMIT 1`,
		customerId, itemId)

	defer rows.Close()

	if err != nil {
		return entities.Purchase{}, err
	}

	var purchase entities.Purchase
	for rows.Next() {
		err := rows.Scan(&purchase.ContentId, &purchase.CustomerId, &purchase.TimeAt, &purchase.Count, &purchase.Price,
			&purchase.Profit)
		if err != nil {
			return entities.Purchase{}, err
		}
	}

	return purchase, err
}

func (r *PurchaseRepository) FindByContentId(ctx context.Context, itemId int, userId int) ([]entities.Purchase, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT *
			   FROM purchases p
			   WHERE p.content_id = $1`,
		itemId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var purchases []entities.Purchase
	for rows.Next() {
		var purchase entities.Purchase
		err := rows.Scan(&purchase.ContentId, &purchase.CustomerId, &purchase.TimeAt, &purchase.Count, &purchase.Price,
			&purchase.Profit)
		if err != nil {
			return nil, err
		}

		purchases = append(purchases, purchase)
	}

	return purchases, err
}

func (r *PurchaseRepository) FindByCustomerId(ctx context.Context, customerId int, userId int) ([]entities.Purchase, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT *
			   FROM purchases p
			   WHERE p.customer_id = $1`,
		customerId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var purchases []entities.Purchase
	for rows.Next() {
		var purchase entities.Purchase
		err := rows.Scan(&purchase.ContentId, &purchase.CustomerId, &purchase.TimeAt, &purchase.Count, &purchase.Price,
			&purchase.Profit)
		if err != nil {
			return nil, err
		}

		purchases = append(purchases, purchase)
	}

	return purchases, err
}

func (r *PurchaseRepository) Add(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime,
	count types.JsonNullInt64, price types.JsonNullInt64, profit types.JsonNullInt64, userId int) error {
	if !timeAt.Valid {
		timeAt.Time = time.Now()
		timeAt.Valid = true
	}

	_, err := r.db.ExecContext(ctx,
		`INSERT INTO purchases (content_id, customer_id, time_at, count, price, profit)
			   VALUES ($1, $2, $3, $4, $5, $6)`,
		itemId, customerId, timeAt, count, price, profit)

	if err != nil {
		return err
	}

	return nil
}

func (r *PurchaseRepository) Delete(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime, userId int) error {
	id := -1
	if !timeAt.Valid {
		err := r.db.QueryRowContext(ctx,
			`DELETE
				   FROM purchases p
				   WHERE p.customer_id = $1
					 AND p.content_id = $2 
				   RETURNING p.content_id`,
			customerId, itemId).Scan(&id)

		return err
	} else {
		_, err := r.db.ExecContext(ctx,
			`DELETE
				   FROM purchases p
				   WHERE p.customer_id = $1
					 AND p.content_id = $2
					 AND p.time_at = $3`,
			customerId, itemId, timeAt)

		return err
	}
}

func (r *PurchaseRepository) DeleteByCustomerId(ctx context.Context, customerId int, userId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE
			   FROM purchases p
			   WHERE p.customer_id = $1`,
		customerId)

	return err
}

func (r *PurchaseRepository) DeleteByItemId(ctx context.Context, itemId int, userId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE
			   FROM purchases p
			   WHERE p.content_id = $1`,
		itemId)

	return err
}
