package purchase

import (
	"context"
	"diploma/entities"
	"diploma/types"
)

type Repository interface {
	Get(ctx context.Context, itemId int, customerId int, userId int) (entities.Purchase, error)
	FindByContentId(ctx context.Context, itemId int, userId int) ([]entities.Purchase, error)
	FindByCustomerId(ctx context.Context, customerId int, userId int) ([]entities.Purchase, error)
	Add(ctx context.Context, itemId int, customerId int, time types.JsonNullTime, count types.JsonNullInt64,
		price types.JsonNullInt64, profit types.JsonNullInt64, userId int) error
	Delete(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime, userId int) error
	DeleteByCustomerId(ctx context.Context, customerId int, userId int) error
	DeleteByItemId(ctx context.Context, itemId int, userId int) error
}
