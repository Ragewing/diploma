package purchase

import (
	"context"
	"diploma/entities"
	"diploma/types"
)

type Usecase interface {
	Add(ctx context.Context, userId int, modelName string, contentId string, customerId string,
		time types.JsonNullTime, count types.JsonNullInt64, price types.JsonNullInt64,
		profit types.JsonNullInt64) (entities.Purchase, error)
	GetByContentId(ctx context.Context, contentId string, modelName string, userId int) ([]entities.Purchase, error)
	GetByCustomerId(ctx context.Context, customerId string, userId int) ([]entities.Purchase, error)
	Delete(ctx context.Context, customerId string, contentId string, timeAt types.JsonNullTime, modelName string,
		userId int) error
	DeleteByCustomerId(ctx context.Context, customerId string, userId int) error
	DeleteByItemId(ctx context.Context, content string, modelName string, userId int) error
}
