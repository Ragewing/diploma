package usecase

import (
	"context"
	"diploma/content"
	"diploma/contentModel"
	"diploma/customer"
	"diploma/entities"
	"diploma/purchase"
	"diploma/types"
)

type PurchaseUsecase struct {
	purchaseRepository purchase.Repository
	contentRepository  content.Repository
	customerRepository customer.Repository
	modelRepository    contentModel.Repository
}

func NewPurchaseUsecase(purchaseRepo purchase.Repository, contentRepo content.Repository,
	customerRepo customer.Repository, modelRepo contentModel.Repository) *PurchaseUsecase {
	return &PurchaseUsecase{
		purchaseRepository: purchaseRepo,
		contentRepository:  contentRepo,
		customerRepository: customerRepo,
		modelRepository:    modelRepo,
	}
}

func (u PurchaseUsecase) Add(ctx context.Context, userId int, modelName string, contentId string, customerId string,
	time types.JsonNullTime, count types.JsonNullInt64, price types.JsonNullInt64,
	profit types.JsonNullInt64) (entities.Purchase, error) {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return entities.Purchase{}, err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return entities.Purchase{}, err
	}

	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return entities.Purchase{}, err
	}

	err = u.purchaseRepository.Add(ctx, itemId, customerLocalId, time, count, price, profit, userId)
	if err != nil {
		return entities.Purchase{}, err
	}

	return u.purchaseRepository.Get(ctx, itemId, customerLocalId, userId)
}

func (u PurchaseUsecase) GetByContentId(ctx context.Context, contentId string, modelName string,
	userId int) ([]entities.Purchase, error) {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return nil, err
	}

	return u.purchaseRepository.FindByContentId(ctx, itemId, userId)
}

func (u PurchaseUsecase) GetByCustomerId(ctx context.Context, customerId string, userId int) ([]entities.Purchase, error) {
	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return nil, err
	}

	return u.purchaseRepository.FindByCustomerId(ctx, customerLocalId, userId)
}

func (u PurchaseUsecase) Delete(ctx context.Context, customerId string, contentId string, timeAt types.JsonNullTime,
	modelName string, userId int) error {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return err
	}

	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return err
	}

	return u.purchaseRepository.Delete(ctx, itemId, customerLocalId, timeAt, userId)
}

func (u PurchaseUsecase) DeleteByCustomerId(ctx context.Context, customerId string, userId int) error {
	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return err
	}

	return u.purchaseRepository.DeleteByCustomerId(ctx, customerLocalId, userId)
}

func (u PurchaseUsecase) DeleteByItemId(ctx context.Context, content string, modelName string, userId int) error {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, content, modelId)
	if err != nil {
		return err
	}

	return u.purchaseRepository.DeleteByItemId(ctx, itemId, userId)
}
