package http

import (
	"diploma/purchase"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, purchaseUC purchase.Usecase, userUC user.Usecase) {
	h := NewHandler(purchaseUC, userUC)

	views := router.Group("/purchases")
	{
		views.POST("", h.Add)
		views.GET("/byContentId", h.GetPurchasesByContentId)
		views.GET("/byCustomerId", h.GetPurchasesByCustomerId)
		views.DELETE("", h.DeleteViews)
	}
}
