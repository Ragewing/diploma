package http

import (
	"diploma/contentModel"
	"diploma/entities"
	"diploma/purchase"
	"diploma/types"
	"diploma/user"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	useCase purchase.Usecase
	userUC  user.Usecase
}

func NewHandler(usecase purchase.Usecase, userUC user.Usecase) *Handler {
	return &Handler{
		useCase: usecase,
		userUC:  userUC,
	}
}

type addPurchaseInput struct {
	ModelName  string              `json:"model_name" binding:"required"`
	UserKey    string              `json:"user_key" binding:"required"`
	ContentId  string              `json:"content_id" binding:"required"`
	CustomerId string              `json:"customer_id" binding:"required"`
	TimeAt     types.JsonNullTime  `json:"time_at"`
	Count      types.JsonNullInt64 `json:"count"`
	Price      types.JsonNullInt64 `json:"price"`
	Profit     types.JsonNullInt64 `json:"profit"`
}

type addPurchaseResponse struct {
	Purchase entities.Purchase `json:"purchase"`
	Error    string            `json:"error"`
}

func (h *Handler) Add(c *gin.Context) {
	var inp addPurchaseInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	addedPurchase, err := h.useCase.Add(ctx, foundUser.Id, inp.ModelName, inp.ContentId, inp.CustomerId,
		inp.TimeAt, inp.Count, inp.Price, inp.Profit)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &addPurchaseResponse{
		Purchase: addedPurchase,
		Error:    errMsg,
	})
}

type getPurchasesResponse struct {
	Purchases []entities.Purchase `json:"views"`
	Error     string              `json:"error"`
}

func (h *Handler) GetPurchasesByContentId(c *gin.Context) {
	contentId := c.Query("contentId")
	modelId := c.Query("modelName")
	key := c.Query("key")

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	purchases, err := h.useCase.GetByContentId(ctx, contentId, modelId, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getPurchasesResponse{
		Purchases: purchases,
		Error:     errMsg,
	})
}

func (h *Handler) GetPurchasesByCustomerId(c *gin.Context) {
	customerId := c.Query("customerId")
	key := c.Query("key")

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	purchases, err := h.useCase.GetByCustomerId(ctx, customerId, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getPurchasesResponse{
		Purchases: purchases,
		Error:     errMsg,
	})
}

type deletePurchases struct {
	UserKey    string             `json:"user_key" binding:"required"`
	ModelName  string             `json:"model_id" binding:"required"`
	CustomerId string             `json:"customer_id" binding:"required"`
	ContentId  string             `json:"content_id" binding:"required"`
	TimeAt     types.JsonNullTime `json:"time_at"`
}

type deletePurchasesResponse struct {
	Error string `json:"error"`
}

func (h *Handler) DeleteViews(c *gin.Context) {
	var inp deletePurchases
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	err = h.useCase.Delete(ctx, inp.CustomerId, inp.ContentId, inp.TimeAt, inp.ModelName, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &deletePurchasesResponse{
		Error: errMsg,
	})
}
