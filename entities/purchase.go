package entities

import "diploma/types"

type Purchase struct {
	ContentId  int
	CustomerId int
	TimeAt     types.JsonNullTime
	Count      types.JsonNullInt64
	Price      types.JsonNullInt64
	Profit     types.JsonNullInt64
}
