package entities

import (
	"diploma/types"
)

type TypeValuePair struct {
	AttributeId     int                  `json:"attribute_id" binding:"required"`
	AttributeValue  types.JsonNullString `json:"attribute_value" binding:"required"`
	AttributeTypeId int                  `json:"-"`
}

type ContentData struct {
	Data []TypeValuePair `json:"data" binding:"required"`
}
