package entities

type Attribute struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type Model struct {
	Id         int         `json:"id"`
	Name       string      `json:"name"`
	Attributes []Attribute `json:"attributes"`
}
