package entities

import "diploma/types"

type Rating struct {
	ContentId  int                `json:"content_id"`
	CustomerId int                `json:"customer_id"`
	Rating     float64            `json:"rating"`
	TimeAt     types.JsonNullTime `json:"time_at"`
}
