package entities

import (
	"diploma/types"
)

type View struct {
	ContentId  int
	CustomerId int
	TimeAt     types.JsonNullTime
	Duration   types.JsonNullInt64
}
