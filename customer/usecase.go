package customer

import (
	"context"
	"diploma/entities"
)

type Usecase interface {
	Get(ctx context.Context, userId int) ([]entities.Customer, error)
	Add(ctx context.Context, customerId string, userId int) (entities.Customer, error)
	Delete(ctx context.Context, customerId string, userId int) (entities.Customer, error)
}
