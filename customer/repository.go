package customer

import (
	"context"
	"diploma/entities"
)

type Repository interface {
	GetByUserId(ctx context.Context, userId int) ([]entities.Customer, error)
	GetByCustomerLocalId(ctx context.Context, customerId int) (entities.Customer, error)
	GetCustomerLocalId(ctx context.Context, customerId string, userId int) (int, error)
	Add(ctx context.Context, customerId string, userId int) (int, error)
	Delete(ctx context.Context, customerId string, userId int) (entities.Customer, error)
}
