package usecase

import (
	"context"
	"diploma/customer"
	"diploma/entities"
)

type CustomerUsecase struct {
	customerRepository customer.Repository
}

func NewContentUsecase(customerRepo customer.Repository) *CustomerUsecase {
	return &CustomerUsecase{
		customerRepository: customerRepo,
	}
}

func (u CustomerUsecase) Get(ctx context.Context, userId int) ([]entities.Customer, error) {
	return u.customerRepository.GetByUserId(ctx, userId)
}

func (u CustomerUsecase) Add(ctx context.Context, customerId string, userId int) (entities.Customer, error) {
	customerLocalId, err := u.customerRepository.Add(ctx, customerId, userId)
	if err != nil {
		return entities.Customer{}, err
	}

	return u.customerRepository.GetByCustomerLocalId(ctx, customerLocalId)
}

func (u CustomerUsecase) Delete(ctx context.Context, customerId string, userId int) (entities.Customer, error) {
	return u.customerRepository.Delete(ctx, customerId, userId)
}
