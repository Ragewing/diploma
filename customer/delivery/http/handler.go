package http

import (
	"diploma/contentModel"
	"diploma/customer"
	"diploma/entities"
	"diploma/user"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	customerUC customer.Usecase
	userUC     user.Usecase
}

func NewHandler(customerUsecase customer.Usecase, userUsecase user.Usecase) *Handler {
	return &Handler{
		customerUC: customerUsecase,
		userUC:     userUsecase,
	}
}

type getCustomersResponse struct {
	Customers []entities.Customer `json:"customers"`
	Error     string              `json:"error"`
}

func (h *Handler) GetCustomers(c *gin.Context) {
	key := c.Query("key")
	if key == "" {
		_ = c.AbortWithError(http.StatusForbidden, errors.New(""))
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	customers, err := h.customerUC.Get(ctx, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getCustomersResponse{
		Customers: customers,
		Error:     errMsg,
	})
}

type createCustomerInput struct {
	UserKey    string `json:"user_key" binding:"required"`
	CustomerId string `json:"customer_id" binding:"required"`
}

type createCustomerResponse struct {
	CustomerId string `json:"customer_id"`
	Error      string `json:"error"`
}

func (h *Handler) AddCustomer(c *gin.Context) {
	var inp createCustomerInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}
	newCustomer, err := h.customerUC.Add(ctx, inp.CustomerId, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &createCustomerResponse{
		CustomerId: newCustomer.CustomerId,
		Error:      errMsg,
	})
}

type deleteCustomerInput struct {
	CustomerId string `json:"customer_id" binding:"required"`
	UserKey    string `json:"user_key" binding:"required"`
}

type deleteCustomerResponse struct {
	CustomerId string `json:"customer_id"`
	Error      string `json:"error"`
}

func (h *Handler) DeleteCustomer(c *gin.Context) {
	var inp deleteCustomerInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	deletedCustomer, err := h.customerUC.Delete(ctx, inp.CustomerId, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &deleteCustomerResponse{
		CustomerId: deletedCustomer.CustomerId,
		Error:      errMsg,
	})
}
