package http

import (
	"diploma/customer"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, customerUC customer.Usecase, userUC user.Usecase) {
	h := NewHandler(customerUC, userUC)

	customers := router.Group("/customers")
	{
		customers.GET("/:userId", h.GetCustomers)
		customers.POST("", h.AddCustomer)
		customers.DELETE("", h.DeleteCustomer)
	}
}
