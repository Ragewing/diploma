package postgre

import (
	"context"
	"database/sql"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"log"
	"regexp"
	"testing"
)

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestFindByModelId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewCustomerRepository(conn)

	userId := 2

	query := "SELECT c.customer_global_id " +
		"FROM customers c " +
		"WHERE c.user_id = $1"

	rows := sqlmock.NewRows([]string{"c.customer_global_id"}).
		AddRow("Cntve9Q0ZnKZKEp7")

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(userId).WillReturnRows(rows)

	customer, err := repo.GetByUserId(ctx, userId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestGetByCustomerLocalId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewCustomerRepository(conn)

	customerId := 1

	query := "SELECT c.customer_global_id " +
		"FROM customers c " +
		"WHERE c.customer_id = $1"

	rows := sqlmock.NewRows([]string{"c.customer_global_id"}).
		AddRow("2jbHhCtNfKAldVAB")

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(customerId).WillReturnRows(rows)

	customer, err := repo.GetByCustomerLocalId(ctx, 1)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestGetCustomerLocalId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewCustomerRepository(conn)

	customerId := "2jbHhCtNfKAldVAB"
	userId := 1

	query := "SELECT c.customer_id " +
		"FROM customers c " +
		"WHERE c.customer_global_id = $1 " +
		"AND c.user_id = $2"

	rows := sqlmock.NewRows([]string{"c.customer_id"}).
		AddRow(1)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(customerId, userId).WillReturnRows(rows)

	customer, err := repo.GetCustomerLocalId(ctx, customerId, userId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestAdd(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewCustomerRepository(conn)

	customerId := "Fkh5fMZsdQJ9bEc5"
	userId := 2

	query := "INSERT INTO customers (customer_global_id, user_id) VALUES ($1, $2) RETURNING customer_id"

	rows := sqlmock.NewRows([]string{"customer_id"}).
		AddRow(16)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(customerId, userId).WillReturnRows(rows)

	customer, err := repo.Add(ctx, customerId, userId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}
