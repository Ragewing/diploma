package postgre

import (
	"context"
	"database/sql"
	"diploma/entities"
)

type CustomerRepository struct {
	db *sql.Conn
}

func NewCustomerRepository(db *sql.Conn) *CustomerRepository {
	return &CustomerRepository{db: db}
}

func (r *CustomerRepository) GetByUserId(ctx context.Context, userId int) ([]entities.Customer, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT c.customer_global_id
			   FROM customers c
			   WHERE c.user_id = $1`,
		userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var customers []entities.Customer
	for rows.Next() {
		var customer entities.Customer

		err := rows.Scan(&customer.CustomerId)
		if err != nil {
			return nil, err
		}

		customers = append(customers, customer)
	}

	return customers, nil
}

func (r *CustomerRepository) GetByCustomerLocalId(ctx context.Context, customerId int) (entities.Customer, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT c.customer_global_id
			   FROM customers c
			   WHERE c.customer_id = $1`,
		customerId)

	defer rows.Close()

	if err != nil {
		return entities.Customer{}, err
	}

	var customer entities.Customer
	for rows.Next() {
		err := rows.Scan(&customer.CustomerId)
		if err != nil {
			return entities.Customer{}, err
		}
	}

	return customer, nil
}

func (r *CustomerRepository) GetCustomerLocalId(ctx context.Context, customerId string, userId int) (int, error) {
	id := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT c.customer_id
			   FROM customers c
			   WHERE c.customer_global_id = $1
			     AND c.user_id = $2`,
		customerId, userId).Scan(&id)

	if err != nil {
		return -1, err
	}

	return id, nil
}

func (r *CustomerRepository) Add(ctx context.Context, customerId string, userId int) (int, error) {
	id := 0
	err := r.db.QueryRowContext(ctx,
		`INSERT INTO customers (customer_global_id, user_id) VALUES ($1, $2) RETURNING customer_id`,
		customerId, userId).Scan(&id)

	if err != nil {
		return -1, err
	}

	return id, nil
}

func (r *CustomerRepository) Delete(ctx context.Context, customerId string, userId int) (entities.Customer, error) {
	panic("not implemented!")
}
