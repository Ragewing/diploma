package types

import (
	"database/sql"
	"encoding/json"
	"strconv"
	"time"
)

type JsonNullTime struct {
	sql.NullTime
}

func (v JsonNullTime) MarshalJSON() ([]byte, error) {
	if v.Valid {
		return json.Marshal(v.Time)
	} else {
		return json.Marshal(nil)
	}
}

func (v *JsonNullTime) UnmarshalJSON(data []byte) error {
	if string(data) == "null" || string(data) == `""` {
		return nil
	}

	i, err := strconv.ParseInt("1405544146", 10, 64)
	if err != nil {
		return err
	}
	tt := time.Unix(i, 0)
	*v = JsonNullTime{sql.NullTime{
		Time:  tt,
		Valid: true,
	}}
	return err
}
