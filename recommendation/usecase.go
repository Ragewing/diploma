package recommendation

import "context"

type Usecase interface {
	FindRecommendationsForUser(ctx context.Context, customerId string, modelName string, recCount int, userId int) ([]string, error)
}
