package http

import (
	"diploma/recommendation"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, recommendationUC recommendation.Usecase, userUC user.Usecase) {
	h := NewHandler(recommendationUC, userUC)

	recommendations := router.Group("/recommendations")
	{
		recommendations.GET("", h.GetRecommendations)
	}
}
