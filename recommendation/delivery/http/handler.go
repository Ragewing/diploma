package http

import (
	"diploma/contentModel"
	"diploma/recommendation"
	"diploma/user"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type Handler struct {
	recommendationUC recommendation.Usecase
	userUC           user.Usecase
}

func NewHandler(recUC recommendation.Usecase, userUC user.Usecase) *Handler {
	return &Handler{
		recommendationUC: recUC,
		userUC:           userUC,
	}
}

type getRecommendationResponse struct {
	ContentIds []string `json:"content"`
	Error      string   `json:"error"`
}

func (h *Handler) GetRecommendations(c *gin.Context) {
	key := c.Query("key")
	if key == "" {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}
	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	modelName := c.Query("modelName")
	customerId := c.Query("customerId")
	if modelName == "" || customerId == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]string{
			"error": recommendation.ErrQueryNotFilled.Error(),
		})
	}
	recCount, err := strconv.Atoi(c.Query("count"))
	if err != nil {
		recCount = 10
	}

	recommendations, err := h.recommendationUC.FindRecommendationsForUser(ctx, customerId, modelName, recCount, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getRecommendationResponse{
		ContentIds: recommendations,
		Error:      errMsg,
	})
}
