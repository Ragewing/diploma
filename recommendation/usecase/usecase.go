package usecase

import (
	"bufio"
	"context"
	"database/sql"
	"diploma/content"
	"diploma/contentModel"
	"diploma/customer"
	"diploma/math/argsort"
	"diploma/rating"
	"diploma/types"
	"diploma/user"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"os/exec"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"unicode"
)

type RecommendationUsecase struct {
	contentRepository  content.Repository
	userRepository     user.Repository
	modelRepository    contentModel.Repository
	ratingRepository   rating.Repository
	customerRepository customer.Repository
}

func NewRecommendationUsecase(contentRepo content.Repository, userRepo user.Repository,
	modelRepo contentModel.Repository, ratingRepo rating.Repository, customerRepo customer.Repository) *RecommendationUsecase {
	return &RecommendationUsecase{
		contentRepository:  contentRepo,
		userRepository:     userRepo,
		modelRepository:    modelRepo,
		ratingRepository:   ratingRepo,
		customerRepository: customerRepo,
	}
}

func removeSpaces(str string) string {
	var b strings.Builder
	b.Grow(len(str))
	for _, ch := range str {
		if !unicode.IsSpace(ch) {
			b.WriteRune(ch)
		}
	}
	return b.String()
}

func getIdx(arr []string, elem string) int {
	for idx, val := range arr {
		if val == elem {
			return idx
		}
	}
	return -1
}

func cosineSim(a, b []float64) (float64, error) {
	if len(a) != len(b) {
		return -2, errors.New("Slices length should be equal")
	}

	sumAB := .0
	sumSA := .0
	sumSB := .0
	for idx := range a {
		sumAB += a[idx] * b[idx]
		sumSA += a[idx] * a[idx]
		sumSB += b[idx] * b[idx]
	}

	return sumAB / (math.Sqrt(sumSA) * math.Sqrt(sumSB)), nil
}

func (r *RecommendationUsecase) FindRecommendationsForUser(ctx context.Context, customerId, modelName string,
	recCount int, userId int) ([]string, error) {
	modelId, err := r.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	attrCount, err := r.modelRepository.GetAttributesCount(ctx, modelId)
	if err != nil {
		return nil, err
	}
	itemsCount, err := r.contentRepository.GetItemsCount(ctx, modelId)
	if err != nil {
		return nil, err
	}

	items, err := r.contentRepository.FindByModelId(ctx, modelId, attrCount, itemsCount, userId)
	if err != nil {
		return nil, err
	}

	model, err := r.modelRepository.FindByModelId(ctx, modelId, userId)
	if err != nil {
		return nil, err
	}

	customerLocId, err := r.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return nil, err
	}

	sets := make(map[int]map[string]bool)

	setsIds := make([]int, 0)
	for _, attr := range model.Attributes {
		if attr.Type == "set" {
			sets[attr.Id] = map[string]bool{}
			setsIds = append(setsIds, attr.Id)
		}
	}

	integerColumns := make(map[int][]int)
	floatColumns := make(map[int][]float64)
	for itemIdx := range items {
		for valIdx, val := range items[itemIdx].Data {
			if val.AttributeTypeId == 2 {
				clean := removeSpaces(strings.ToLower(val.AttributeValue.String))
				items[itemIdx].Data[valIdx].AttributeValue = types.JsonNullString{NullString: sql.NullString{String: clean}}
				sets[val.AttributeId][clean] = true
			} else if val.AttributeTypeId == 3 {
				var parsedNum int
				if val.AttributeValue.String == "" {
					parsedNum = 0
				} else {
					parsedNum, err = strconv.Atoi(val.AttributeValue.String)
					if err != nil {
						return nil, err
					}
				}
				integerColumns[val.AttributeId] = append(integerColumns[val.AttributeId], parsedNum)
			} else if val.AttributeTypeId == 4 {
				var parsedNum float64
				if val.AttributeValue.String == "" {
					parsedNum = .0
				} else {
					parsedNum, err = strconv.ParseFloat(val.AttributeValue.String, 64)
					if err != nil {
						return nil, err
					}
					floatColumns[val.AttributeId] = append(floatColumns[val.AttributeId], parsedNum)
				}
			}
		}
	}

	sortedSets := make(map[int][]string)
	for _, setId := range setsIds {
		for setVal := range sets[setId] {
			sortedSets[setId] = append(sortedSets[setId], setVal)
		}
	}

	matrixWidth := len(integerColumns) + len(floatColumns)
	for _, setId := range setsIds {
		sort.Strings(sortedSets[setId])
		matrixWidth += len(sortedSets[setId])
	}

	itemsMatrix := make([][]float64, len(items))
	for i := range itemsMatrix {
		itemsMatrix[i] = make([]float64, matrixWidth)
	}
	for itemIdx, item := range items {
		for _, val := range item.Data {
			if val.AttributeTypeId == 2 {
				setGlobIdx := 0
				for _, set := range sortedSets {
					for _, setVal := range set {
						if setVal == val.AttributeValue.String {
							if stValIdx := getIdx(set, setVal); stValIdx != -1 {
								itemsMatrix[itemIdx][setGlobIdx+stValIdx] = 1.
								continue
							}
						}
					}
					setGlobIdx = len(set)
				}
			}
		}
	}

	// Normalization
	matEnd := matrixWidth - (len(integerColumns) + len(floatColumns))
	for intIdx := 0; intIdx < len(integerColumns); intIdx++ {
		for columnKey := range integerColumns {
			column := integerColumns[columnKey]
			min := column[0]
			max := column[0]
			for _, c := range column {
				if c > max {
					max = c
				}
				if c < min {
					min = c
				}
			}
			for i := 0; i < len(column); i++ {
				itemsMatrix[i][matEnd+intIdx] = float64(column[i]-min) / float64(max-min)
			}
		}
	}
	matEnd = matEnd + len(integerColumns)
	for floatIdx := 0; floatIdx < len(floatColumns); floatIdx++ {
		for columnKey := range floatColumns {
			column := floatColumns[columnKey]
			min := column[0]
			max := column[0]
			for _, c := range column {
				if c > max {
					max = c
				}
				if c < min {
					min = c
				}
			}
			for i := 0; i < len(itemsMatrix); i++ {
				itemsMatrix[i][matEnd+floatIdx] = (column[i] - min) / (max - min)
			}
		}
	}

	similarityMat := make([][]float64, len(items))
	for i := range similarityMat {
		similarityMat[i] = make([]float64, len(items))
	}
	for aRowIdx := range itemsMatrix {
		for bRowIdx := aRowIdx; bRowIdx < len(items); bRowIdx++ {
			similarity, err := cosineSim(itemsMatrix[aRowIdx], itemsMatrix[bRowIdx])
			if err != nil {
				return nil, err
			}
			similarityMat[aRowIdx][bRowIdx] = similarity
			similarityMat[bRowIdx][aRowIdx] = similarity
		}
	}

	for i := 0; i < 20; i++ {
		for j := 0; j < 20; j++ {
			fmt.Printf("%3.2f ", similarityMat[i][j])
		}
		fmt.Println()
	}

	inputItemIdx := 0
	similarItems := similarityMat[inputItemIdx]
	selfItemIdx := sliceIndex(len(similarItems), func(i int) bool {
		if i == inputItemIdx {
			return true
		}
		return false
	})

	similarItems[selfItemIdx] = similarItems[len(similarItems)-1]
	similarItems = similarItems[:len(similarItems)-1]

	order := argsort.Sort(sort.Float64Slice(similarItems))
	reverseAny(order)

	var recCountN int
	if 2*recCount >= len(order) {
		recCountN = len(order)
	} else {
		recCountN = 2 * recCount
	}

	recommendations := order[:recCountN]
	var recIdsStr string
	recIds := make([]string, recCountN)
	for i, recIdx := range recommendations {
		recIds[i] = items[recIdx].Data[1].AttributeValue.String
		recIdsStr += items[recIdx].Data[0].AttributeValue.String + ","
	}
	recIdsStr = strings.TrimRight(recIdsStr, ",")

	ratings, err := r.ratingRepository.FindByModelId(ctx, modelId, userId)
	if err != nil {
		return nil, err
	}

	filename := "./python/temp_csv/" + strconv.Itoa(userId) + modelName + ".csv"
	file, err := os.Create(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	err = writer.Write([]string{"customerId", "itemId", "rating"})
	if err != nil {
		return nil, err
	}
	for _, rt := range ratings {
		err = writer.Write([]string{strconv.Itoa(rt.CustomerId), strconv.Itoa(rt.ContentId), fmt.Sprintf("%.2f", rt.Rating)})
		if err != nil {
			return nil, err
		}
	}

	cmd := exec.Command("python", "./python/svd.py", filename, recIdsStr, strconv.Itoa(customerLocId))
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		panic(err)
	}
	err = cmd.Start()
	if err != nil {
		panic(err)
	}

	copyOutput(stderr)
	//copyOutput(stdout)
	combined := getOrderFromPy(stdout)
	cmd.Wait()

	combined = combined[:recCount]
	var combinedIds []string
	for _, c := range combined {
		combinedIds = append(combinedIds, recIds[c])
	}

	return combinedIds, nil
}

func getOrderFromPy(r io.Reader) []int {
	var order []int
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		text := scanner.Text()
		if strings.Index(text, "[") != -1 {
			text = strings.Trim(text, "[]")
			splited := strings.Split(text, ",")
			for i := 0; i < len(splited)-1; i++ {
				idxParsed, err := strconv.Atoi(splited[i])
				if err != nil {
					_ = 1
				}
				order = append(order, idxParsed)
			}
		}
	}

	return order
}

func reverseAny(s interface{}) {
	n := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, n-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

func sliceIndex(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

func copyOutput(r io.Reader) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}
