package recommendation

import "errors"

var ErrQueryNotFilled = errors.New("Не усі параметри запиту задано")
