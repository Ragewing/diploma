package content

import (
	"context"
	"diploma/entities"
)

type Usecase interface {
	Get(ctx context.Context, modelName string, userId int) ([]entities.ContentData, error)
	Create(ctx context.Context, modelName string, contentId string, userId int) (*entities.ContentData, error)
	SetContentData(ctx context.Context, modelName string, contentId string, data entities.ContentData,
		userId int) (*entities.ContentData, error)
}
