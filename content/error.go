package content

import "errors"

var ErrAttributesCountMismatch = errors.New("Кількість атрибутів не співпадає")
var ErrItemNotFound = errors.New("Контент з заданим ідентифікатором не знайдено")
