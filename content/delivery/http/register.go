package http

import (
	"diploma/content"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, contentUC content.Usecase, userUC user.Usecase) {
	h := NewHandler(contentUC, userUC)

	contentModel := router.Group("/content")
	{
		contentModel.POST("", h.Create)
		contentModel.PUT("/:contentId", h.SetContentData)
		contentModel.GET("", h.GetItems)
	}
}
