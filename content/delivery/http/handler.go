package http

import (
	"diploma/content"
	"diploma/contentModel"
	"diploma/entities"
	"diploma/user"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	contentUC content.Usecase
	userUC    user.Usecase
}

func NewHandler(usecase content.Usecase, userUC user.Usecase) *Handler {
	return &Handler{
		contentUC: usecase,
		userUC:    userUC,
	}
}

type setContentDataInput struct {
	ModelName   string               `json:"model_name" binding:"required"`
	ContentData entities.ContentData `json:"data" binding:"required"`
	UserKey     string               `json:"user_key" binding:"required"`
}

type setContentDataResponse struct {
	Item  *entities.ContentData `json:"item"`
	Error string                `json:"error"`
}

func (h *Handler) SetContentData(c *gin.Context) {
	contentId := c.Param("contentId")

	var inp setContentDataInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	item, err := h.contentUC.SetContentData(ctx, inp.ModelName, contentId, inp.ContentData, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &setContentDataResponse{
		Item:  item,
		Error: errMsg,
	})
}

type createContentInput struct {
	ContentId string `json:"content_id" binding:"required"`
	ModelName string `json:"model_name" binding:"required"`
	UserKey   string `json:"user_key" binding:"required"`
}

type createContentResponse struct {
	Item  *entities.ContentData `json:"item"`
	Error string                `json:"error"`
}

func (h *Handler) Create(c *gin.Context) {
	var inp createContentInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	model, err := h.contentUC.Create(ctx, inp.ModelName, inp.ContentId, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &createContentResponse{
		Item:  model,
		Error: errMsg,
	})
}

type getContentResponse struct {
	Content []entities.ContentData `json:"usecase"`
	Error   string                 `json:"error"`
}

func (h *Handler) GetItems(c *gin.Context) {
	modelName := c.Query("modelName")

	key := c.Query("key")
	if key == "" {
		_ = c.AbortWithError(http.StatusForbidden, errors.New(""))
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	items, err := h.contentUC.Get(ctx, modelName, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getContentResponse{
		Content: items,
		Error:   errMsg,
	})
}
