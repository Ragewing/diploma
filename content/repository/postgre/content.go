package postgre

import (
	"context"
	"database/sql"
	"diploma/content"
	"diploma/entities"
	"strconv"
	"strings"
)

type ContentRepository struct {
	db *sql.Conn
}

func NewContentRepository(db *sql.Conn) *ContentRepository {
	return &ContentRepository{db: db}
}

func (r *ContentRepository) FindByModelId(ctx context.Context, modelId, attributesCount, itemsCount, userId int) ([]entities.ContentData, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT itvl.attribute_id, itvl.value, a.type_id, COUNT(itvl.item_id) OVER (PARTITION BY itvl.item_id)
			   FROM items_values itvl,
				    items it,
			        attributes a,
			        content_models cm
			   WHERE it.item_id = itvl.item_id
			     AND it.model_id = $1
                 AND a.attribute_id = itvl.attribute_id
                 AND cm.model_id = it.model_id
                 AND cm.user_id = $2
			   ORDER BY itvl.item_id, itvl.attribute_id`,
		modelId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var content = make([]entities.ContentData, itemsCount)
	itemIdx, attributeIdx := 0, 0
	type ExtendedData struct {
		data       entities.TypeValuePair
		attrsCount int
	}
	for rows.Next() {
		var data ExtendedData

		err := rows.Scan(&data.data.AttributeId, &data.data.AttributeValue, &data.data.AttributeTypeId, &data.attrsCount)
		if err != nil {
			return nil, err
		}

		attributeIdx++
		content[itemIdx].Data = append(content[itemIdx].Data, data.data)
		if attributeIdx == data.attrsCount {
			attributeIdx = 0
			itemIdx++
		}
	}

	return content, nil
}

func (r *ContentRepository) FindByItemId(ctx context.Context, itemId int) (*entities.ContentData, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT itvl.attribute_id, itvl.value
			   FROM items_values itvl,
				    items it
			   WHERE it.item_id = itvl.item_id
			     AND it.item_id = $1
			   ORDER BY itvl.item_id, itvl.attribute_id`,
		itemId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	item := new(entities.ContentData)
	for rows.Next() {
		var data entities.TypeValuePair

		err := rows.Scan(&data.AttributeId, &data.AttributeValue)
		if err != nil {
			return nil, err
		}

		item.Data = append(item.Data, data)
	}

	return item, nil
}

func (r *ContentRepository) Create(ctx context.Context, modelId int, contentId string, userId int) (int, error) {
	var itemId int
	err := r.db.QueryRowContext(ctx,
		`SELECT * FROM create_item($1, $2, $3)`,
		modelId, contentId, userId).Scan(&itemId)

	if err != nil {
		return -1, err
	}

	return itemId, nil
}

func (r *ContentRepository) GetItemsCount(ctx context.Context, modelId int) (int, error) {
	count := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT COUNT(i.item_id)
			   FROM items i
			   WHERE i.model_id = $1`,
		modelId).Scan(&count)

	if err != nil {
		return -1, err
	}

	return count, nil
}

func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)
	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}
	return old
}

func (r *ContentRepository) SetItemData(ctx context.Context, itemId int, data entities.ContentData) error {
	sqlStr := "INSERT INTO items_values (item_id, attribute_id, value) VALUES "
	var vals []interface{}

	for _, row := range data.Data {
		sqlStr += "(?, ?, ?),"
		vals = append(vals, itemId, row.AttributeId, row.AttributeValue.String)
	}

	sqlStr = strings.TrimSuffix(sqlStr, ",")

	sqlStr = ReplaceSQL(sqlStr, "?")

	stmt, err := r.db.PrepareContext(ctx, sqlStr)
	if err != nil {
		return err
	}

	_, err = stmt.ExecContext(ctx, vals...)
	if err != nil {
		return err
	}

	return nil
}

func (r *ContentRepository) GetItemIdByContentId(ctx context.Context, contentId string, modelId int) (int, error) {
	var itemId int
	err := r.db.QueryRowContext(ctx,
		`SELECT i.item_id
			   FROM items_values itvl,
				    items i
			   WHERE itvl.attribute_id = 1
			     AND itvl.item_id = i.item_id
			     AND i.model_id = $1
			     AND itvl.value = $2`,
		modelId, contentId).Scan(&itemId)

	if err != nil {
		return -1, content.ErrItemNotFound
	}

	return itemId, nil
}

func (r *ContentRepository) Delete(ctx context.Context, contentId int, modelId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE FROM items WHERE item_id = $1 AND model_id = $2`,
		contentId, modelId)

	return err
}
