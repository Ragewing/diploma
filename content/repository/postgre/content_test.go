package postgre

import (
	"context"
	"database/sql"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"log"
	"regexp"
	"testing"
)

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestFindByModelId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewContentRepository(conn)

	modelId := 3

	query := "SELECT itvl.attribute_id, itvl.value " +
		"FROM items_values itvl, " +
		"     items it " +
		"WHERE it.item_id = itvl.item_id " +
		"  AND it.model_id = $1 " +
		"ORDER BY itvl.item_id, itvl.attribute_id"

	rows := sqlmock.NewRows([]string{"itvl.attribute_id", "itvl.value"}).
		AddRow(1, "wYkwNYNAtMMEJz9q").
		AddRow(2, "Властелин колец").
		AddRow(1, "I6Ahev9Q1CMmABcQ").
		AddRow(2, "Братва и кольцо").
		AddRow(1, "MugOKWuS8dIEnrDu").
		AddRow(2, "ТестТСЕТ")

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId).WillReturnRows(rows)

	customer, err := repo.FindByModelId(ctx, modelId, 2, 3, 1)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestFindByItemId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewContentRepository(conn)

	itemId := 1

	query := "SELECT itvl.attribute_id, itvl.value " +
		"FROM items_values itvl, " +
		"     items it " +
		"WHERE it.item_id = itvl.item_id " +
		"  AND it.item_id = $1 " +
		"ORDER BY itvl.item_id, itvl.attribute_id"

	rows := sqlmock.NewRows([]string{"itvl.attribute_id", "itvl.value"}).
		AddRow(1, "KCubOVXDGcVrzFZQ").
		AddRow(2, "Зеленая миля").
		AddRow(6, 16).
		AddRow(7, 1999)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(itemId).WillReturnRows(rows)

	customer, err := repo.FindByItemId(ctx, itemId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestCreate(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewContentRepository(conn)

	modelId := 1
	contentId := "l6cWAEKh70JWO6PK"

	query := "SELECT * FROM create_item($1, $2)"

	rows := sqlmock.NewRows([]string{"_item_id"}).
		AddRow(20)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId, contentId).WillReturnRows(rows)

	customer, err := repo.Create(ctx, modelId, contentId, 1)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestGetItemsCount(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewContentRepository(conn)

	modelId := 1

	query := "SELECT COUNT(i.item_id) " +
		"FROM items i " +
		"WHERE i.model_id = $1"

	rows := sqlmock.NewRows([]string{"COUNT(i.item_id)"}).
		AddRow(5)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId).WillReturnRows(rows)

	customer, err := repo.GetItemsCount(ctx, modelId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestGetItemIdByContentId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewContentRepository(conn)

	modelId := 3
	itemId := "MugOKWuS8dIEnrDu"

	query := "SELECT i.item_id " +
		"FROM items_values itvl, " +
		"	   items i " +
		"WHERE itvl.attribute_id = 1 " +
		"  AND itvl.item_id = i.item_id " +
		"  AND i.model_id = $1 " +
		"  AND itvl.value = $2"

	rows := sqlmock.NewRows([]string{"i.item_id"}).
		AddRow(7).
		AddRow(8).
		AddRow(9).
		AddRow(17).
		AddRow(6)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId, itemId).WillReturnRows(rows)

	customer, err := repo.GetItemIdByContentId(ctx, itemId, modelId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}
