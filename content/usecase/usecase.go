package usecase

import (
	"context"
	"diploma/content"
	"diploma/contentModel"
	"diploma/entities"
	"github.com/lib/pq"
)

type ContentUsecase struct {
	contentRepository      content.Repository
	contentModelRepository contentModel.Repository
}

func NewContentUsecase(contentRepo content.Repository, contentModelRepo contentModel.Repository) *ContentUsecase {
	return &ContentUsecase{
		contentRepository:      contentRepo,
		contentModelRepository: contentModelRepo,
	}
}

func (u ContentUsecase) Get(ctx context.Context, modelName string, userId int) ([]entities.ContentData, error) {
	modelId, err := u.contentModelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	attributesCount, err := u.contentModelRepository.GetAttributesCount(ctx, modelId)
	if err != nil {
		return nil, err
	}

	itemsCount, err := u.contentRepository.GetItemsCount(ctx, modelId)
	if err != nil {
		return nil, err
	}

	return u.contentRepository.FindByModelId(ctx, modelId, attributesCount, itemsCount, userId)
}

func (u ContentUsecase) Create(ctx context.Context, modelName string, contentId string,
	userId int) (*entities.ContentData, error) {
	modelId, err := u.contentModelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	itemId, err := u.contentRepository.Create(ctx, modelId, contentId, userId)
	if err != nil {
		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == ("77008") {
				return nil, contentModel.ErrModelNotFound
			}
		}
		return nil, err
	}

	return u.contentRepository.FindByItemId(ctx, itemId)
}

func (u ContentUsecase) SetContentData(ctx context.Context, modelName string, contentId string,
	data entities.ContentData, userId int) (*entities.ContentData, error) {
	modelId, err := u.contentModelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	model, err := u.contentModelRepository.FindByModelId(ctx, modelId, userId)
	if err != nil {
		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == ("77009") {
				return nil, contentModel.ErrModelNotFound
			}
		}
		return nil, err
	}

	copy(model.Attributes[0:], model.Attributes[1:])
	model.Attributes[len(model.Attributes)-1] = entities.Attribute{}
	model.Attributes = model.Attributes[:len(model.Attributes)-1]

	isSet := false
	if len(data.Data) > len(model.Attributes) {
		for _, d := range data.Data {
			typeId, err := u.contentModelRepository.GetAttributeType(ctx, modelId, d.AttributeId)
			if err != nil {
				return nil, err
			}
			if typeId == 2 {
				isSet = true
				break
			}
		}

		if !isSet {
			return nil, content.ErrAttributesCountMismatch
		}
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return nil, err
	}

	err = u.contentRepository.SetItemData(ctx, itemId, data)
	if err != nil {
		return nil, err
	}

	return u.contentRepository.FindByItemId(ctx, itemId)
}
