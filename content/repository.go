package content

import (
	"context"
	"diploma/entities"
)

type Repository interface {
	FindByModelId(ctx context.Context, modelId, attributesCount, itemsCount, userId int) ([]entities.ContentData, error)
	FindByItemId(ctx context.Context, itemId int) (*entities.ContentData, error)
	Create(ctx context.Context, modelId int, contentId string, userId int) (int, error)
	GetItemsCount(ctx context.Context, modelId int) (int, error)
	SetItemData(ctx context.Context, itemId int, data entities.ContentData) error
	GetItemIdByContentId(ctx context.Context, contentId string, modelId int) (int, error)
	Delete(ctx context.Context, contentId int, modelId int) error
}
