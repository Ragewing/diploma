package server

import (
	"context"
	"database/sql"
	_ "diploma/app/docs"
	"diploma/content"
	contentHTTP "diploma/content/delivery/http"
	contentPostgre "diploma/content/repository/postgre"
	contentUsecase "diploma/content/usecase"
	"diploma/contentModel"
	contentModelHTTP "diploma/contentModel/delivery/http"
	contentModelPostgre "diploma/contentModel/repository/postgre"
	contentModelUsecase "diploma/contentModel/usecase"
	"diploma/customer"
	customerHTTP "diploma/customer/delivery/http"
	customerPostgre "diploma/customer/repository/postgre"
	customerUsecase "diploma/customer/usecase"
	"diploma/purchase"
	purchaseHTTP "diploma/purchase/delivery/http"
	purchasePostgre "diploma/purchase/repository/postgre"
	purchaseUsecase "diploma/purchase/usecase"
	"diploma/rating"
	ratingHTTP "diploma/rating/delivery/http"
	ratingPostgre "diploma/rating/repository/postgre"
	ratingUsecase "diploma/rating/usecase"
	"diploma/recommendation"
	recommendationHTTP "diploma/recommendation/delivery/http"
	recommendationUsecase "diploma/recommendation/usecase"
	pagesHTTP "diploma/site"
	"diploma/user"
	userPostgre "diploma/user/repository/postgre"
	userUsecase "diploma/user/usecase"
	"diploma/view"
	viewHTTP "diploma/view/delivery/http"
	viewPostgre "diploma/view/repository/postgre"
	viewUsecase "diploma/view/usecase"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

type App struct {
	httpServer *http.Server

	contentModelUC   contentModel.Usecase
	customerUC       customer.Usecase
	contentUC        content.Usecase
	viewUC           view.Usecase
	purchaseUC       purchase.Usecase
	userUC           user.Usecase
	ratingUC         rating.Usecase
	recommendationUC recommendation.Usecase
}

func NewApp() *App {
	db := initDB()

	contentModelRepo := contentModelPostgre.NewModelRepository(db)
	customerRepo := customerPostgre.NewCustomerRepository(db)
	contentRepo := contentPostgre.NewContentRepository(db)
	viewRepo := viewPostgre.NewViewRepository(db)
	purchaseRepo := purchasePostgre.NewPurchaseRepository(db)
	userRepo := userPostgre.NewUserRepository(db)
	ratingRepo := ratingPostgre.NewRatingRepository(db)

	return &App{
		contentModelUC: contentModelUsecase.NewModelUsecase(contentModelRepo),
		customerUC:     customerUsecase.NewContentUsecase(customerRepo),
		contentUC:      contentUsecase.NewContentUsecase(contentRepo, contentModelRepo),
		viewUC:         viewUsecase.NewViewUsecase(viewRepo, contentRepo, customerRepo, contentModelRepo),
		purchaseUC:     purchaseUsecase.NewPurchaseUsecase(purchaseRepo, contentRepo, customerRepo, contentModelRepo),
		userUC:         userUsecase.NewUserUsecase(userRepo),
		ratingUC:       ratingUsecase.NewRatingUsecase(ratingRepo, contentRepo, customerRepo, contentModelRepo),
		recommendationUC: recommendationUsecase.NewRecommendationUsecase(contentRepo, userRepo, contentModelRepo,
			ratingRepo, customerRepo),
	}
}

func (a *App) Run(port string) error {
	router := gin.Default()
	router.Use(
		gin.Recovery(),
	)

	router.LoadHTMLGlob("templates/*.tmpl")

	grp := router.Group("/static")
	{
		grp.Static("/js", "./templates/js/")
	}

	url := ginSwagger.URL("http://localhost:5051/swagger/doc.json") // The url pointing to API definition
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	contentModelHTTP.RegisterHTTPEndpoint(router, a.contentModelUC, a.userUC)
	customerHTTP.RegisterHTTPEndpoint(router, a.customerUC, a.userUC)
	contentHTTP.RegisterHTTPEndpoint(router, a.contentUC, a.userUC)
	viewHTTP.RegisterHTTPEndpoint(router, a.viewUC, a.userUC)
	purchaseHTTP.RegisterHTTPEndpoint(router, a.purchaseUC, a.userUC)
	pagesHTTP.RegisterHTTPEndpoint(router, a.contentModelUC, a.userUC)
	ratingHTTP.RegisterHTTPEndpoint(router, a.ratingUC, a.userUC)
	recommendationHTTP.RegisterHTTPEndpoint(router, a.recommendationUC, a.userUC)

	a.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		if err := a.httpServer.ListenAndServe(); err != nil {
			log.Fatalf("Failed to listen and serve: %+v", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	return a.httpServer.Shutdown(ctx)
}

func initDB() *sql.Conn {
	client, err := sql.Open("postgres", viper.GetString("postgre.uri"))
	if err != nil {
		log.Fatalf("Error occured while establishing connection to PostgreSQL: %s", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	conn, err := client.Conn(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = conn.PingContext(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	return conn
}
