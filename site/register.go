package site

import (
	"diploma/contentModel"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, modelUC contentModel.Usecase, userUC user.Usecase) {
	h := NewHandler(modelUC, userUC)

	pages := router.Group("")
	{
		pages.GET("/modelsPage", h.GetModelsPage)
		pages.GET("/", h.SignIn)
		pages.POST("/", h.LogIn)
		pages.GET("/signUp", h.SignUp)
		pages.POST("/signUp", h.Register)
	}
}
