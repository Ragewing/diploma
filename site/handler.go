package site

import (
	"diploma/contentModel"
	"diploma/user"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	modelsUC contentModel.Usecase
	usersUC  user.Usecase
}

func NewHandler(modelsUC contentModel.Usecase, usersUC user.Usecase) *Handler {
	return &Handler{
		modelsUC: modelsUC,
		usersUC:  usersUC,
	}
}

func (h *Handler) GetModelsPage(c *gin.Context) {
	userKey, err := c.Cookie("key")
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()

	foundUser, err := h.usersUC.GetByKey(ctx, userKey)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	if foundUser.Id == 0 {
		c.Redirect(http.StatusSeeOther, "/")
	}

	models, err := h.modelsUC.Get(ctx, foundUser.Id)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.HTML(http.StatusOK, "modelsPage.tmpl", gin.H{"Models": models, "Title": "Моделі", "Key": foundUser.Key})
}

func (h *Handler) SignIn(c *gin.Context) {
	c.HTML(http.StatusOK, "signInPage.tmpl", gin.H{})
}

func (h *Handler) LogIn(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")

	foundUser, err := h.usersUC.GetByEmailPass(c.Request.Context(), email, password)
	if err != nil {
		c.HTML(http.StatusBadRequest, "signInPage.tmpl", gin.H{"Error": err.Error()})
		return
	}
	if foundUser.Id <= 0 {
		c.HTML(http.StatusBadRequest, "signInPage.tmpl", gin.H{"Error": errors.New("Користувача не знайдено")})
		return
	}

	c.SetCookie("key", foundUser.Key, 60*60*24, "/", "/localhost", false, false)
	c.Redirect(http.StatusFound, "/modelsPage")
}

func (h *Handler) SignUp(c *gin.Context) {
	_, err := c.Cookie("key")
	if err == nil {
		c.Redirect(http.StatusFound, "/modelsPage")
		return
	}

	c.HTML(http.StatusOK, "signUpPage.tmpl", gin.H{})
}

func (h *Handler) Register(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")

	foundUser, err := h.usersUC.Create(c.Request.Context(), email, password)
	if err != nil {
		c.HTML(http.StatusBadRequest, "signUpPage.tmpl", gin.H{"Error": err.Error()})
		return
	}

	c.SetCookie("key", foundUser.Key, 60*60*24, "/", "/localhost", false, false)
	c.Redirect(http.StatusFound, "/modelsPage")
}
