package http

import (
	"diploma/user"
	"diploma/view"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, viewUC view.Usecase, userUC user.Usecase) {
	h := NewHandler(viewUC, userUC)

	views := router.Group("/views")
	{
		views.POST("", h.Add)
		views.GET("/byContentId", h.GetViewsByContentId)
		views.GET("/byCustomerId", h.GetViewsByCustomerId)
		views.DELETE("", h.DeleteViews)
	}
}
