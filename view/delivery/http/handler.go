package http

import (
	"diploma/contentModel"
	"diploma/entities"
	"diploma/types"
	"diploma/user"
	"diploma/view"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	useCase view.Usecase
	userUC  user.Usecase
}

func NewHandler(usecase view.Usecase, userUC user.Usecase) *Handler {
	return &Handler{
		useCase: usecase,
		userUC:  userUC,
	}
}

type addViewsInput struct {
	ModelName  string              `json:"model_name" binding:"required"`
	UserKey    string              `json:"user_key" binding:"required"`
	ContentId  string              `json:"content_id" binding:"required"`
	CustomerId string              `json:"customer_id" binding:"required"`
	TimeAt     types.JsonNullTime  `json:"time_at"`
	Duration   types.JsonNullInt64 `json:"duration"`
}

type addViewResponse struct {
	View  entities.View `json:"view"`
	Error string        `json:"error"`
}

func (h *Handler) Add(c *gin.Context) {
	var inp addViewsInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, inp.UserKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	addedView, err := h.useCase.Add(ctx, foundUser.Id, inp.ModelName, inp.ContentId, inp.CustomerId,
		inp.TimeAt, inp.Duration)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &addViewResponse{
		View:  addedView,
		Error: errMsg,
	})
}

type getViewsResponse struct {
	Views []entities.View `json:"views"`
	Error string          `json:"error"`
}

func (h *Handler) GetViewsByContentId(c *gin.Context) {
	contentId := c.Query("contentId")
	modelId := c.Query("modelName")
	key := c.Query("key")

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	views, err := h.useCase.GetByContentId(ctx, contentId, modelId, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getViewsResponse{
		Views: views,
		Error: errMsg,
	})
}

func (h *Handler) GetViewsByCustomerId(c *gin.Context) {
	customerId := c.Query("customerId")
	key := c.Query("key")

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	views, err := h.useCase.GetByCustomerId(c.Request.Context(), customerId, foundUser.Id)

	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getViewsResponse{
		Views: views,
		Error: errMsg,
	})
}

type deleteViews struct {
	UserId     int                `json:"user_id" binding:"required"`
	ModelName  string             `json:"model_id" binding:"required"`
	CustomerId string             `json:"customer_id" binding:"required"`
	ContentId  string             `json:"content_id" binding:"required"`
	TimeAt     types.JsonNullTime `json:"time_at"`
}

type deleteViewsResponse struct {
	Error string `json:"error"`
}

func (h *Handler) DeleteViews(c *gin.Context) {
	var inp deleteViews
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := h.useCase.Delete(c.Request.Context(), inp.CustomerId, inp.ContentId, inp.TimeAt, inp.ModelName, inp.UserId)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &deleteViewsResponse{
		Error: errMsg,
	})
}
