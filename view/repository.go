package view

import (
	"context"
	"diploma/entities"
	"diploma/types"
)

type Repository interface {
	Get(ctx context.Context, itemId int, customerId int, userId int) (entities.View, error)
	FindByContentId(ctx context.Context, itemId, userId int) ([]entities.View, error)
	FindByCustomerId(ctx context.Context, customerId int, userId int) ([]entities.View, error)
	Add(ctx context.Context, itemId int, customerId int, time types.JsonNullTime, duration types.JsonNullInt64) error
	Delete(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime) error
	DeleteByCustomerId(ctx context.Context, customerId int) error
	DeleteByItemId(ctx context.Context, itemId int) error
}
