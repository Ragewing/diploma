package usecase

import (
	"context"
	"diploma/content"
	"diploma/contentModel"
	"diploma/customer"
	"diploma/entities"
	"diploma/types"
	"diploma/view"
)

type ViewUsecase struct {
	viewRepository     view.Repository
	contentRepository  content.Repository
	customerRepository customer.Repository
	modelRepository    contentModel.Repository
}

func NewViewUsecase(viewRepo view.Repository, contentRepo content.Repository, customerRepo customer.Repository,
	modelRepo contentModel.Repository) *ViewUsecase {
	return &ViewUsecase{
		viewRepository:     viewRepo,
		contentRepository:  contentRepo,
		customerRepository: customerRepo,
		modelRepository:    modelRepo,
	}
}

func (u ViewUsecase) Add(ctx context.Context, userId int, modelName string, contentId string, customerId string,
	time types.JsonNullTime, duration types.JsonNullInt64) (entities.View, error) {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return entities.View{}, err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return entities.View{}, err
	}

	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return entities.View{}, err
	}

	err = u.viewRepository.Add(ctx, itemId, customerLocalId, time, duration)
	if err != nil {
		return entities.View{}, err
	}

	return u.viewRepository.Get(ctx, itemId, customerLocalId, userId)
}

func (u ViewUsecase) GetByContentId(ctx context.Context, contentId string, modelName string,
	userId int) ([]entities.View, error) {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return nil, err
	}

	return u.viewRepository.FindByContentId(ctx, itemId, userId)
}

func (u ViewUsecase) GetByCustomerId(ctx context.Context, customerId string, userId int) ([]entities.View, error) {
	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return nil, err
	}

	return u.viewRepository.FindByCustomerId(ctx, customerLocalId, userId)
}

func (u ViewUsecase) Delete(ctx context.Context, customerId string, contentId string, timeAt types.JsonNullTime,
	modelName string, userId int) error {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, contentId, modelId)
	if err != nil {
		return err
	}

	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return err
	}

	return u.viewRepository.Delete(ctx, itemId, customerLocalId, timeAt)
}

func (u ViewUsecase) DeleteByCustomerId(ctx context.Context, customerId string, userId int) error {
	customerLocalId, err := u.customerRepository.GetCustomerLocalId(ctx, customerId, userId)
	if err != nil {
		return err
	}

	return u.viewRepository.DeleteByCustomerId(ctx, customerLocalId)
}

func (u ViewUsecase) DeleteByItemId(ctx context.Context, content string, modelName string, userId int) error {
	modelId, err := u.modelRepository.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	itemId, err := u.contentRepository.GetItemIdByContentId(ctx, content, modelId)
	if err != nil {
		return err
	}

	return u.viewRepository.DeleteByItemId(ctx, itemId)
}
