package postgre

import (
	"context"
	"database/sql"
	"diploma/entities"
	"diploma/types"
	"time"
)

type ViewRepository struct {
	db *sql.Conn
}

func NewViewRepository(db *sql.Conn) *ViewRepository {
	return &ViewRepository{db: db}
}

func (r *ViewRepository) Get(ctx context.Context, itemId int, customerId, userId int) (entities.View, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT v.*
			   FROM views v,
				    items i,
				    content_models cm
			   WHERE v.customer_id = $1
			     AND v.content_id = $2
			     AND i.item_id = v.content_id
			     AND i.model_id = cm.model_id
			     AND cm.user_id = $3
			   ORDER BY time_at DESC`,
		customerId, itemId, userId)

	defer rows.Close()

	if err != nil {
		return entities.View{}, err
	}

	var view entities.View
	for rows.Next() {
		err := rows.Scan(&view.ContentId, &view.CustomerId, &view.TimeAt, &view.Duration)
		if err != nil {
			return entities.View{}, err
		}
	}

	return view, err
}

func (r *ViewRepository) FindByContentId(ctx context.Context, itemId, userId int) ([]entities.View, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT v.*
			   FROM views v,
			        items i,
				    content_models cm
			   WHERE v.content_id = $1
			     AND i.item_id = v.content_id
			     AND i.model_id = cm.model_id
			     AND cm.user_id = $2
			   ORDER BY time_at DESC`,
		itemId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var views []entities.View
	for rows.Next() {
		var view entities.View
		err := rows.Scan(&view.ContentId, &view.CustomerId, &view.TimeAt, &view.Duration)
		if err != nil {
			return nil, err
		}

		views = append(views, view)
	}

	return views, err
}

func (r *ViewRepository) FindByCustomerId(ctx context.Context, customerId, userId int) ([]entities.View, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT *
			   FROM views v,
			        items i,
				    content_models cm
			   WHERE v.customer_id = $1
			     AND i.item_id = v.content_id
			     AND i.model_id = cm.model_id
			     AND cm.user_id = $2
			   ORDER BY time_at DESC`,
		customerId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var views []entities.View
	for rows.Next() {
		var view entities.View
		err := rows.Scan(&view.ContentId, &view.CustomerId, &view.TimeAt, &view.Duration)
		if err != nil {
			return nil, err
		}

		views = append(views, view)
	}

	return views, err
}

func (r *ViewRepository) Add(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime,
	duration types.JsonNullInt64) error {
	if !timeAt.Valid {
		timeAt.Time = time.Now()
		timeAt.Valid = true
	}

	_, err := r.db.ExecContext(ctx,
		`INSERT INTO views (content_id, customer_id, time_at, duration)
			   VALUES ($1, $2, $3, $4)`,
		itemId, customerId, timeAt, duration)

	if err != nil {
		return err
	}

	return nil
}

func (r *ViewRepository) Delete(ctx context.Context, itemId int, customerId int, timeAt types.JsonNullTime) error {
	id := -1
	if !timeAt.Valid {
		err := r.db.QueryRowContext(ctx,
			`DELETE
				   FROM views v
				   WHERE v.customer_id = $1
					 AND v.content_id = $2 
				   RETURNING v.content_id`,
			customerId, itemId).Scan(&id)

		return err
	} else {
		_, err := r.db.ExecContext(ctx,
			`DELETE
				   FROM views v
				   WHERE v.customer_id = $1
					 AND v.content_id = $2
					 AND v.time_at = $3`,
			customerId, itemId, timeAt)

		return err
	}
}

func (r *ViewRepository) DeleteByCustomerId(ctx context.Context, customerId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE
			   FROM views v
			   WHERE v.customer_id = $1`,
		customerId)

	return err
}

func (r *ViewRepository) DeleteByItemId(ctx context.Context, itemId int) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE
			   FROM views v
			   WHERE v.content_id = $1`,
		itemId)

	return err
}
