package postgre

import (
	"context"
	"database/sql"
	"diploma/types"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"log"
	"regexp"
	"testing"
	"time"
)

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestGet(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	customerId := 1
	contentId := 1

	query := "SELECT * " +
		"FROM views v " +
		"WHERE v.customer_id = $1 " +
		"AND v.content_id = $2 " +
		"ORDER BY time_at DESC " +
		"LIMIT 1"

	rows := sqlmock.NewRows([]string{"content_id", "customer_id", "time_at", "duration"}).
		AddRow(1, 1, time.Now(), 15)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(customerId, contentId).WillReturnRows(rows)

	customer, err := repo.Get(ctx, contentId, customerId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestFindByContentId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	contentId := 1

	query := "SELECT * " +
		"FROM views v " +
		"WHERE v.content_id = $1"

	rows := sqlmock.NewRows([]string{"content_id", "customer_id", "time_at", "duration"}).
		AddRow(1, 1, time.Now(), 15)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(contentId).WillReturnRows(rows)

	customer, err := repo.FindByContentId(ctx, contentId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestFindByCustomerId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	customerId := 1

	query := "SELECT * " +
		"FROM views v " +
		"WHERE v.customer_id = $1"

	rows := sqlmock.NewRows([]string{"content_id", "customer_id", "time_at", "duration"}).
		AddRow(1, 1, time.Now(), 15)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(customerId).WillReturnRows(rows)

	customer, err := repo.FindByCustomerId(ctx, customerId)
	assert.NotNil(t, customer)
	assert.NoError(t, err)
}

func TestAdd(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	contentId := 19
	customerId := 1
	timeAt := time.Now()
	duration := int64(60)

	query := "INSERT INTO views (content_id, customer_id, time_at, duration) " +
		"VALUES ($1, $2, $3, $4)"

	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(contentId, customerId, timeAt, duration).WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.Add(ctx, contentId, customerId, types.JsonNullTime{NullTime: sql.NullTime{
		Time:  timeAt,
		Valid: true,
	}},
		types.JsonNullInt64{NullInt64: sql.NullInt64{
			Int64: duration,
			Valid: true,
		}})
	assert.NoError(t, err)
}

func TestDelete(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	customerId := 1
	contentId := 1

	query := "DELETE " +
		"FROM views v " +
		"WHERE v.customer_id = $1 " +
		"  AND v.content_id = $2 " +
		"RETURNING v.content_id"

	rows := sqlmock.NewRows([]string{"v.content_id"}).
		AddRow(19)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(customerId, contentId).WillReturnRows(rows)

	err = repo.Delete(ctx, contentId, customerId, types.JsonNullTime{
		NullTime: sql.NullTime{
			Time:  time.Now(),
			Valid: false,
		}})
	assert.NoError(t, err)
}

func TestDeleteByCustomerId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	customerId := 1

	query := "DELETE " +
		"FROM views v " +
		"WHERE v.customer_id = $1"

	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(customerId).WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.DeleteByCustomerId(ctx, customerId)
	assert.NoError(t, err)
}

func TestDeleteByItemId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewViewRepository(conn)

	contentId := 1

	query := "DELETE " +
		"FROM views v " +
		"WHERE v.content_id = $1"

	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(contentId).WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.DeleteByItemId(ctx, contentId)
	assert.NoError(t, err)
}
