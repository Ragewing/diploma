package contentModel

import (
	"context"
	"diploma/entities"
)

type Repository interface {
	FindByModelId(ctx context.Context, modelId int, userId int) (*entities.Model, error)
	FindByUserId(ctx context.Context, userId int) ([]int, error)
	GetModelName(ctx context.Context, modelId int) (string, error)
	Create(ctx context.Context, name string, userId int) (int, error)
	AddAttribute(ctx context.Context, modelId int, name string, typeId int) error
	RemoveAttribute(ctx context.Context, modelId int, attributeId int) error
	UpdateAttribName(ctx context.Context, modelId, attributeId, userId int, name string) error
	GetAttributesCount(ctx context.Context, modelId int) (int, error)
	FindByModelIdUserId(ctx context.Context, modelId int, userId int) (int, error)
	UpdateModelName(ctx context.Context, modelId, userId int, name string) error
	RemoveModel(ctx context.Context, modelId, userId int) error
	GetAttributeType(ctx context.Context, modelId, attributeId int) (int, error)
	FindModelIdByName(ctx context.Context, userId int, modelName string) (int, error)
}
