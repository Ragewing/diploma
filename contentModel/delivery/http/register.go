package http

import (
	"diploma/contentModel"
	"diploma/user"
	"github.com/gin-gonic/gin"
)

func RegisterHTTPEndpoint(router *gin.Engine, modelUC contentModel.Usecase, userUC user.Usecase) {
	h := NewHandler(modelUC, userUC)

	contentModel := router.Group("/contentModel")
	{
		contentModel.GET("", h.GetModels)
		contentModel.POST("", h.CreateContentModel)
		contentModel.PATCH("", h.UpdateName)
		contentModel.GET("/:modelName", h.GetModelById)
		contentModel.DELETE("/:modelName", h.Delete)
	}

	attributes := router.Group("/attribute")
	{
		attributes.POST("", h.AddAttribute)
		attributes.DELETE("", h.RemoveAttribute)
		attributes.PATCH("", h.UpdateAttribName)
	}
}
