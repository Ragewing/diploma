package http

import (
	"diploma/contentModel"
	"diploma/entities"
	"diploma/user"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type Handler struct {
	modelUC contentModel.Usecase
	userUC  user.Usecase
}

func NewHandler(modelUC contentModel.Usecase, userUC user.Usecase) *Handler {
	return &Handler{
		modelUC: modelUC,
		userUC:  userUC,
	}
}

type getModelsResponse struct {
	Model []*entities.Model `json:"models"`
	Error string            `json:"error"`
}

func (h *Handler) GetModels(c *gin.Context) {
	key := c.Query("key")
	if key == "" {
		_ = c.AbortWithError(http.StatusForbidden, errors.New("У вас немає прав доступу"))
		return
	}

	ctx := c.Request.Context()
	foundUser, err := h.userUC.GetByKey(ctx, key)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	models, err := h.modelUC.Get(ctx, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getModelsResponse{
		Model: models,
		Error: errMsg,
	})
}

type createContentModelInput struct {
	Name    string `json:"name" binding:"required"`
	UserKey string `json:"user_key"`
}

type createContentModelResponse struct {
	Model *entities.Model `json:"contentModel"`
	Error string          `json:"error"`
}

func (h *Handler) CreateContentModel(c *gin.Context) {
	userKey, err := c.Cookie("key")
	inp := new(createContentModelInput)
	if err := c.BindJSON(inp); err != nil {
		if inp.UserKey == "" && userKey == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if userKey == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	ctx := c.Request.Context()
	if userKey == "" && inp.UserKey != "" {
		userKey = inp.UserKey
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	model, err := h.modelUC.Create(c.Request.Context(), inp.Name, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &createContentModelResponse{
		Model: model,
		Error: errMsg,
	})
}

type addAttributeInput struct {
	ModelName string `json:"model_name" binding:"required"`
	Name      string `json:"name"     binding:"required"`
	TypeId    int    `json:"type_id"  binding:"required"`
	UserKey   string `json:"user_key"`
}

type addAttributeResponse struct {
	Model *entities.Model `json:"contentModel"`
	Error string          `json:"error"`
}

func (h *Handler) AddAttribute(c *gin.Context) {
	userKey, err := c.Cookie("key")
	inp := new(addAttributeInput)
	if err := c.ShouldBindJSON(inp); err != nil {
		if inp.UserKey == "" && userKey == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if userKey == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	if len(strings.TrimSpace(inp.Name)) == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrEmptyAttributeName.Error(),
		})
		return
	}

	ctx := c.Request.Context()
	if userKey == "" && inp.UserKey != "" {
		userKey = inp.UserKey
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	model, err := h.modelUC.AddAttribute(ctx, inp.ModelName, inp.Name, inp.TypeId, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &addAttributeResponse{
		Model: model,
		Error: errMsg,
	})
}

type removeAttributeInput struct {
	ModelName   string `json:"model_name"   binding:"required"`
	AttributeId int    `json:"attribute_id" binding:"required"`
	UserKey     string `json:"user_key"`
}

type removeAttributeResponse struct {
	Model *entities.Model `json:"contentModel"`
	Error string          `json:"error"`
}

func (h *Handler) RemoveAttribute(c *gin.Context) {
	userKey, err := c.Cookie("key")
	inp := new(removeAttributeInput)
	if err := c.BindJSON(inp); err != nil {
		if inp.UserKey == "" && userKey == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if userKey == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	ctx := c.Request.Context()
	if userKey == "" && inp.UserKey != "" {
		userKey = inp.UserKey
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	model, err := h.modelUC.RemoveAttribute(ctx, inp.ModelName, inp.AttributeId, foundUser.Id)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &removeAttributeResponse{
		Model: model,
		Error: errMsg,
	})
}

type getModelResponse struct {
	Model *entities.Model `json:"contentModel"`
	Error string          `json:"error"`
}

func (h *Handler) GetModelById(c *gin.Context) {
	key := c.Query("key")

	userKey, err := c.Cookie("key")
	if err != nil {
		if userKey == "" && key == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if key == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	modelName := c.Param("modelName")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	ctx := c.Request.Context()
	if userKey == "" && key != "" {
		userKey = key
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		_ = c.AbortWithError(http.StatusForbidden, contentModel.ErrModelNoRights)
		return
	}

	model, err := h.modelUC.GetByName(ctx, foundUser.Id, modelName)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getModelResponse{
		Model: model,
		Error: errMsg,
	})
}

type updateModelNameInput struct {
	Name      string `json:"name" binding:"required"`
	ModelName string `json:"model_name" binding:"required"`
	UserKey   string `json:"user_key"`
}

func (h *Handler) UpdateName(c *gin.Context) {
	userKey, err := c.Cookie("key")
	inp := new(updateModelNameInput)
	if err := c.ShouldBindJSON(inp); err != nil {
		if inp.UserKey == "" && userKey == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if userKey == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	ctx := c.Request.Context()
	if userKey == "" && inp.UserKey != "" {
		userKey = inp.UserKey
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	model, err := h.modelUC.UpdateName(ctx, inp.ModelName, foundUser.Id, inp.Name)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &getModelResponse{
		Model: model,
		Error: errMsg,
	})
}

type removeModelInput struct {
	UserKey string `json:"user_key"`
}

type removeModelResponse struct {
	Error string `json:"error"`
}

func (h *Handler) Delete(c *gin.Context) {
	userKey, err := c.Cookie("key")
	inp := new(removeModelInput)
	if err := c.ShouldBindJSON(inp); err != nil {
		if inp.UserKey == "" && userKey == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if userKey == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	modelName := c.Param("modelName")

	ctx := c.Request.Context()
	if userKey == "" && inp.UserKey != "" {
		userKey = inp.UserKey
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	err = h.modelUC.Delete(ctx, modelName, foundUser.Id)
	var errMsg string
	if err != nil {
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &removeModelResponse{
		Error: errMsg,
	})
}

type editAttrNameInput struct {
	ModelName string `json:"model_name" binding:"required"`
	AttribId  int    `json:"attrib_id" binding:"required"`
	Name      string `json:"name" binding:"required"`
	UserKey   string `json:"user_key"`
}

type editAttrNameResponse struct {
	Model *entities.Model `json:"contentModel"`
	Error string          `json:"error"`
}

func (h *Handler) UpdateAttribName(c *gin.Context) {
	userKey, err := c.Cookie("key")
	inp := new(editAttrNameInput)
	if err := c.BindJSON(inp); err != nil {
		if inp.UserKey == "" && userKey == "" {
			_ = c.AbortWithError(http.StatusForbidden, err)
			return
		}
		if userKey == "" {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
	}

	ctx := c.Request.Context()
	if userKey == "" && inp.UserKey != "" {
		userKey = inp.UserKey
	}
	foundUser, err := h.userUC.GetByKey(ctx, userKey)
	if err != nil || foundUser.Id == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{
			"error": contentModel.ErrModelNoRights.Error(),
		})
		return
	}

	model, err := h.modelUC.UpdateAttribName(ctx, inp.ModelName, inp.AttribId, foundUser.Id, inp.Name)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &editAttrNameResponse{
		Model: model,
		Error: errMsg,
	})
}
