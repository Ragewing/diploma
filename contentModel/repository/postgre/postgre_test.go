package postgre

import (
	"context"
	"database/sql"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"log"
	"regexp"
	"testing"
)

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestFindByModelId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	modelId := 1

	query := "SELECT attr.attribute_id, attr.name, t.name " +
		"FROM content_model_attributes cma, " +
		"     attributes attr, " +
		"     types t " +
		"WHERE cma.model_id = $1 " +
		"  AND attr.attribute_id = cma.attribute_id " +
		"  AND t.type_id = attr.type_id"

	rows := sqlmock.NewRows([]string{"attr.attribute_id", "attr.name", "t.name"}).
		AddRow(1, "id", "string").
		AddRow(2, "title", "string").
		AddRow(6, "age", "int").
		AddRow(7, "year", "int")

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId).WillReturnRows(rows)

	model, err := repo.FindByModelId(ctx, modelId)
	assert.NotNil(t, model)
	assert.NoError(t, err)
}

func TestFindByUserId(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	userId := 1
	modelId := 1

	query := "SELECT cm.model_id " +
		"FROM content_models cm " +
		"WHERE cm.user_id = $1"

	rows := sqlmock.NewRows([]string{"cm.model_id"}).
		AddRow(1).
		AddRow(3).
		AddRow(19)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(userId).WillReturnRows(rows)

	model, err := repo.FindByUserId(ctx, modelId)
	assert.NotNil(t, model)
	assert.NoError(t, err)
}

func TestGetModelName(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	modelId := 1

	query := "SELECT cm.name " +
		"FROM content_models cm " +
		"WHERE cm.model_id = $1"

	rows := sqlmock.NewRows([]string{"cm.name"}).
		AddRow("Films")

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId).WillReturnRows(rows)

	model, err := repo.GetModelName(ctx, modelId)
	assert.NotNil(t, model)
	assert.NoError(t, err)
}

func TestCreate(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	userId := 1
	name := "Test"

	query := "INSERT INTO content_models (user_id, name) VALUES ($1, $2) RETURNING model_id"

	rows := sqlmock.NewRows([]string{"modelId"}).
		AddRow(20)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(userId, name).WillReturnRows(rows)

	model, err := repo.Create(ctx, name, userId)
	assert.NotNil(t, model)
	assert.NoError(t, err)
}

func TestAddAttribute(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	typeId := 1
	modelId := 1
	name := "Test"

	query := "CALL add_attribute_to_content_model($1, $2, $3)"
	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(name, typeId, modelId).WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.AddAttribute(ctx, modelId, name, typeId)
	assert.NoError(t, err)
}

func TestRemoveAttribute(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	attributeId := 1
	modelId := 1

	query := "CALL remove_attribute_from_content_model($1, $2)"
	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(attributeId, modelId).WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.RemoveAttribute(ctx, modelId, attributeId)
	assert.NoError(t, err)
}

func TestGetAttributesCount(t *testing.T) {
	db, mock := NewMock()
	ctx := context.Background()
	conn, err := db.Conn(ctx)
	repo := NewModelRepository(conn)

	modelId := 1

	rows := sqlmock.NewRows([]string{"count(*)"}).
		AddRow(4)

	query := "SELECT count(*) " +
		"FROM content_model_attributes cma " +
		"WHERE cma.model_id = $1"
	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(modelId).WillReturnRows(rows)

	count, err := repo.GetAttributesCount(ctx, modelId)
	assert.NotNil(t, count)
	assert.NoError(t, err)
}
