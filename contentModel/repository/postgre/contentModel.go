package postgre

import (
	"context"
	"database/sql"
	"diploma/contentModel"
	"diploma/entities"
)

type ContentModelRepository struct {
	db *sql.Conn
}

func NewModelRepository(db *sql.Conn) *ContentModelRepository {
	return &ContentModelRepository{db: db}
}

func (r *ContentModelRepository) FindByModelId(ctx context.Context, modelId int, userId int) (*entities.Model, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT attr.attribute_id, attr.name, t.name
			   FROM content_model_attributes cma,
			        attributes attr,
			        types t,
			        content_models cm
			   WHERE cma.model_id = $1
			     AND attr.attribute_id = cma.attribute_id
			     AND t.type_id = attr.type_id
			     AND cm.model_id = cma.model_id
			     AND cm.user_id = $2`,
		modelId, userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var model entities.Model
	model.Id = modelId
	for rows.Next() {
		attribute := new(entities.Attribute)
		err := rows.Scan(&attribute.Id, &attribute.Name, &attribute.Type)
		if err != nil {
			return nil, err
		}

		model.Attributes = append(model.Attributes, *attribute)
	}

	model.Name, err = r.GetModelName(ctx, model.Id)
	if err != nil {
		return nil, err
	}

	if model.Name == "" {
		return nil, contentModel.ErrModelNotFound
	}

	return &model, nil
}

func (r *ContentModelRepository) FindByUserId(ctx context.Context, userId int) ([]int, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT cm.model_id
			   FROM content_models cm
			   WHERE cm.user_id = $1
               ORDER BY cm.created_at ASC`,
		userId)

	defer rows.Close()

	if err != nil {
		return nil, err
	}

	var modelIds []int
	for rows.Next() {
		var id int
		err := rows.Scan(&id)
		if err != nil {
			return nil, err
		}

		modelIds = append(modelIds, id)
	}

	return modelIds, nil
}

func (r *ContentModelRepository) GetModelName(ctx context.Context, modelId int) (string, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT cm.name
			   FROM content_models cm
			   WHERE cm.model_id = $1`,
		modelId)

	defer rows.Close()

	if err != nil {
		return "", err
	}

	var modelName string
	for rows.Next() {
		err := rows.Scan(&modelName)
		if err != nil {
			return "", err
		}
	}

	return modelName, nil
}

func (r *ContentModelRepository) Create(ctx context.Context, name string, userId int) (int, error) {
	id := -1
	err := r.db.QueryRowContext(ctx,
		`INSERT INTO content_models (user_id, name) VALUES ($1, $2) RETURNING model_id`,
		userId, name).Scan(&id)

	return id, err
}

func (r *ContentModelRepository) AddAttribute(ctx context.Context, modelId int, name string, typeId int) error {
	_, err := r.db.ExecContext(ctx,
		`CALL add_attribute_to_content_model($1, $2, $3)`,
		name, typeId, modelId)

	return err
}

func (r *ContentModelRepository) RemoveAttribute(ctx context.Context, modelId int, attributeId int) error {
	_, err := r.db.ExecContext(ctx,
		`CALL remove_attribute_from_content_model($1, $2)`,
		attributeId, modelId)

	return err
}

func (r *ContentModelRepository) UpdateAttribName(ctx context.Context, modelId, attributeId, userId int, name string) error {
	_, err := r.db.ExecContext(ctx,
		`CALL update_attrib_name($1, $2, $3, $4)`,
		name, attributeId, modelId, userId)

	return err
}

func (r *ContentModelRepository) GetAttributesCount(ctx context.Context, modelId int) (int, error) {
	count := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT count(*)
			   FROM content_model_attributes cma
			   WHERE cma.model_id = $1`,
		modelId).Scan(&count)

	if err != nil {
		return -1, err
	}

	return count, nil
}

func (r *ContentModelRepository) FindByModelIdUserId(ctx context.Context, modelId int, userId int) (int, error) {
	id := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT cm.model_id
			   FROM content_models cm
			   WHERE cm.model_id = $1
			     AND cm.user_id = $2`,
		modelId, userId).Scan(&id)

	return id, err
}

func (r *ContentModelRepository) UpdateModelName(ctx context.Context, modelId, userId int, name string) error {
	_, err := r.db.ExecContext(ctx,
		`UPDATE content_models SET name = $1 WHERE user_id = $2 AND model_id = $3`,
		name, userId, modelId)

	return err
}

func (r *ContentModelRepository) RemoveModel(ctx context.Context, modelId, userId int) error {
	_, err := r.db.ExecContext(ctx,
		`CALL remove_model($1, $2)`,
		modelId, userId)

	return err
}

func (r *ContentModelRepository) GetAttributeType(ctx context.Context, modelId, attributeId int) (int, error) {
	id := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT a.type_id
			   FROM content_model_attributes cma,
				    attributes a
			   WHERE cma.attribute_id = $1
			     AND cma.model_id = $2
			     AND cma.attribute_id = a.attribute_id`,
		attributeId, modelId).Scan(&id)

	return id, err
}

func (r *ContentModelRepository) FindModelIdByName(ctx context.Context, userId int, modelName string) (int, error) {
	id := -1
	err := r.db.QueryRowContext(ctx,
		`SELECT cm.model_id
			   FROM content_models cm
			   WHERE cm.user_id = $1
			     AND cm.name = $2`,
		userId, modelName).Scan(&id)

	return id, err
}
