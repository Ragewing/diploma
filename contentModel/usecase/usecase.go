package usecase

import (
	"context"
	"diploma/contentModel"
	"diploma/entities"
	"github.com/lib/pq"
)

type ContentModelUsecase struct {
	modelRepo contentModel.Repository
}

func NewModelUsecase(modelRepo contentModel.Repository) *ContentModelUsecase {
	return &ContentModelUsecase{modelRepo: modelRepo}
}

func (u ContentModelUsecase) Get(ctx context.Context, userId int) ([]*entities.Model, error) {
	ids, err := u.modelRepo.FindByUserId(ctx, userId)
	if err != nil {
		return nil, err
	}

	var models []*entities.Model
	for _, id := range ids {
		model, err := u.modelRepo.FindByModelId(ctx, id, userId)
		if err != nil {
			return nil, err
		}

		models = append(models, model)
	}

	return models, nil
}

func (u ContentModelUsecase) GetByName(ctx context.Context, userId int, modelName string) (*entities.Model, error) {
	modelId, err := u.modelRepo.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) Create(ctx context.Context, name string, userId int) (*entities.Model, error) {
	modelId, err := u.modelRepo.Create(ctx, name, userId)
	if err != nil {
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) AddAttribute(ctx context.Context, modelName string, name string, typeId int,
	userId int) (*entities.Model, error) {
	modelId, err := u.modelRepo.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	if err := u.modelRepo.AddAttribute(ctx, modelId, name, typeId); err != nil {
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) RemoveAttribute(ctx context.Context, modelName string, attributeId int,
	userId int) (*entities.Model, error) {
	modelId, err := u.modelRepo.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	if err := u.modelRepo.RemoveAttribute(ctx, modelId, attributeId); err != nil {
		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == ("77007") {
				return nil, contentModel.ErrDeleteIdAttribute
			}
		}
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) UpdateAttribName(ctx context.Context, modelName string, attributeId, userId int,
	name string) (*entities.Model, error) {
	modelId, err := u.modelRepo.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	if err := u.modelRepo.UpdateAttribName(ctx, modelId, attributeId, userId, name); err != nil {
		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == ("77002") {
				return nil, contentModel.ErrModelNoRights
			}
			if pgerr.Code == ("77003") {
				return nil, contentModel.ErrAttributeNotFound
			}
			if pgerr.Code == ("77004") {
				return nil, contentModel.ErrAttributeNotFound
			}
			if pgerr.Code == ("77005") {
				return nil, contentModel.ErrRenameIdAttribute
			}
			if pgerr.Code == ("77006") {
				return nil, contentModel.ErrAttributeExists
			}
		}
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) GetById(ctx context.Context, modelId int, userId int) (*entities.Model, error) {
	modelId, err := u.modelRepo.FindByModelIdUserId(ctx, modelId, userId)
	if err != nil {
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) UpdateName(ctx context.Context, modelName string, userId int,
	name string) (*entities.Model, error) {
	modelId, err := u.modelRepo.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return nil, err
	}

	err = u.modelRepo.UpdateModelName(ctx, modelId, userId, name)
	if err != nil {
		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == ("77001") {
				return nil, contentModel.ErrModelNameExists
			}
		}
		return nil, err
	}

	return u.modelRepo.FindByModelId(ctx, modelId, userId)
}

func (u ContentModelUsecase) Delete(ctx context.Context, modelName string, userId int) error {
	modelId, err := u.modelRepo.FindModelIdByName(ctx, userId, modelName)
	if err != nil {
		return err
	}

	err = u.modelRepo.RemoveModel(ctx, modelId, userId)
	if err != nil {
		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == ("77002") {
				return contentModel.ErrModelNoRights
			}
		}
		return err
	}

	return nil
}
