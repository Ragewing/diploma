package contentModel

import "errors"

var ErrModelNotFound = errors.New("Модель не знайдено")
var ErrAttributeNotFound = errors.New("В даній моделі не існує такого атрибуту")
var ErrModelNameExists = errors.New("Модель з даним ім'ям вже існує")
var ErrModelNoRights = errors.New("У Вас немає доступу до даної моделі")
var ErrRenameIdAttribute = errors.New("Неможливо змінити атрибут ідентифікатору моделі")
var ErrAttributeExists = errors.New("Атрибут з таким ім'ям вже існує")
var ErrDeleteIdAttribute = errors.New("Неможливо видалити атрибут ідентифікатору моделі")
var ErrEmptyAttributeName = errors.New("Неможливо створити атрибут без ім'я")
