package contentModel

import (
	"context"
	"diploma/entities"
)

type Usecase interface {
	Get(ctx context.Context, userId int) ([]*entities.Model, error)
	GetByName(ctx context.Context, userId int, modelName string) (*entities.Model, error)
	Create(ctx context.Context, name string, userId int) (*entities.Model, error)
	AddAttribute(ctx context.Context, modelName string, name string, typeId int, userId int) (*entities.Model, error)
	RemoveAttribute(ctx context.Context, modelName string, attributeId int, userId int) (*entities.Model, error)
	UpdateAttribName(ctx context.Context, modelName string, attributeId, userId int, name string) (*entities.Model, error)
	GetById(ctx context.Context, modelId int, userId int) (*entities.Model, error)
	UpdateName(ctx context.Context, modelName string, userId int, name string) (*entities.Model, error)
	Delete(ctx context.Context, modelName string, userId int) error
}
