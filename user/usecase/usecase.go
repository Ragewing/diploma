package usecase

import (
	"context"
	"crypto/sha1"
	"diploma/entities"
	"diploma/user"
	"encoding/base64"
)

type UserUsecase struct {
	userRepository user.Repository
}

func NewUserUsecase(userRepository user.Repository) *UserUsecase {
	return &UserUsecase{
		userRepository: userRepository,
	}
}

func (u UserUsecase) GetByEmailPass(ctx context.Context, email string, password string) (entities.User, error) {
	id, err := u.userRepository.GetByEmailPass(ctx, email, password)
	if err != nil {
		return entities.User{}, err
	}

	return u.userRepository.GetById(ctx, id)
}

func (u UserUsecase) GetByKey(ctx context.Context, key string) (entities.User, error) {
	id, err := u.userRepository.GetByKey(ctx, key)
	if err != nil {
		return entities.User{}, err
	}

	return u.userRepository.GetById(ctx, id)
}

func (u UserUsecase) Create(ctx context.Context, email string, password string) (entities.User, error) {
	id, err := u.userRepository.GetByEmail(ctx, email)
	if err != nil {
		return entities.User{}, err
	}

	if id != -1 {
		return entities.User{}, user.ErrUserExists
	}

	hasher := sha1.New()
	bv := []byte(email)
	hasher.Write(bv)
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	id, err = u.userRepository.Create(ctx, email, password, sha)
	if err != nil {
		return entities.User{}, err
	}

	return u.userRepository.GetById(ctx, id)
}
