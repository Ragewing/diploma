package postgre

import (
	"context"
	"database/sql"
	"diploma/entities"
)

type UserRepository struct {
	db *sql.Conn
}

func NewUserRepository(db *sql.Conn) *UserRepository {
	return &UserRepository{db: db}
}

func (r *UserRepository) GetByEmailPass(ctx context.Context, email string, password string) (int, error) {
	id := -1
	rows, err := r.db.QueryContext(ctx,
		`SELECT u.user_id
			   FROM users u
			   WHERE u.email = $1
			     AND u.password = $2`,
		email, password)

	defer rows.Close()

	if err != nil {
		return -1, err
	}

	for rows.Next() {
		err = rows.Scan(&id)
	}

	return id, err
}

func (r *UserRepository) GetById(ctx context.Context, id int) (entities.User, error) {
	rows, err := r.db.QueryContext(ctx,
		`SELECT u.user_id, u.email, u.password, u.key
			   FROM users u
			   WHERE u.user_id = $1`,
		id)

	defer rows.Close()

	if err != nil {
		return entities.User{Id: 0}, err
	}

	var user entities.User
	for rows.Next() {
		err = rows.Scan(&user.Id, &user.Email, &user.Password, &user.Key)
	}

	return user, err
}

func (r *UserRepository) GetByEmail(ctx context.Context, email string) (int, error) {
	id := -1
	rows, err := r.db.QueryContext(ctx,
		`SELECT u.user_id
			   FROM users u
			   WHERE u.email = $1`,
		email)

	defer rows.Close()

	if err != nil {
		return id, err
	}

	for rows.Next() {
		err = rows.Scan(&id)
	}

	return id, err
}

func (r *UserRepository) GetByKey(ctx context.Context, key string) (int, error) {
	id := -1
	rows, err := r.db.QueryContext(ctx,
		`SELECT u.user_id
			   FROM users u
			   WHERE u.key = $1`,
		key)

	defer rows.Close()

	if err != nil {
		return -1, err
	}

	for rows.Next() {
		err = rows.Scan(&id)
	}

	return id, err
}

func (r *UserRepository) Create(ctx context.Context, email string, password string, key string) (int, error) {
	id := -1
	err := r.db.QueryRowContext(ctx,
		`INSERT INTO users(email, password, key) VALUES ($1, $2, $3) RETURNING user_id`,
		email, password, key).Scan(&id)

	return id, err
}
