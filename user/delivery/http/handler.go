package http

import (
	"diploma/entities"
	"diploma/user"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handler struct {
	useCase user.Usecase
}

func NewHandler(usecase user.Usecase) *Handler {
	return &Handler{useCase: usecase}
}

type createUserInput struct {
	Email    string `json:"models"`
	Password string `json:"error"`
}

type createUserResponse struct {
	User  entities.User `json:"user"`
	Error string        `json:"error"`
}

func (h *Handler) CreateUser(c *gin.Context) {
	var inp createUserInput
	if err := c.BindJSON(&inp); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	createdUser, err := h.useCase.Create(c.Request.Context(), inp.Email, inp.Password)
	var errMsg string
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		errMsg = err.Error()
	}

	c.JSON(http.StatusOK, &createUserResponse{
		User:  createdUser,
		Error: errMsg,
	})
}
