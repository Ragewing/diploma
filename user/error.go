package user

import "errors"

var ErrUserExists = errors.New("Користувач з такою електронною адресою вже існує")
