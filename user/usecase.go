package user

import (
	"context"
	"diploma/entities"
)

type Usecase interface {
	GetByEmailPass(ctx context.Context, email string, password string) (entities.User, error)
	GetByKey(ctx context.Context, key string) (entities.User, error)
	Create(ctx context.Context, email string, password string) (entities.User, error)
}
