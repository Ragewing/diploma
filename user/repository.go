package user

import (
	"context"
	"diploma/entities"
)

type Repository interface {
	GetByEmailPass(ctx context.Context, email string, password string) (int, error)
	GetByEmail(ctx context.Context, email string) (int, error)
	GetById(ctx context.Context, id int) (entities.User, error)
	GetByKey(ctx context.Context, key string) (int, error)
	Create(ctx context.Context, email string, password string, key string) (int, error)
}
