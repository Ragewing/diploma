$(document).ready(function () {
    $("#logout").click(function () {
        $.removeCookie("key")
        $.removeCookie("models")
        window.location.href = "/";
    })

    $("button")
        .filter(function () {
            return this.id.match(/edit_model_.*/)
        })
        .click(onEditModelClick)
    $("button")
        .filter(function () {
            return this.id.match(/remove_model_.*/)
        })
        .click(onRemoveModelClick)
    $("button")
        .filter(function () {
            return this.id.match(/add_attrib_btn_.*/)
        })
        .click(onAddAttribClick)

    $("#create_model_btn").click(onCreateModelClick)

    $("#confirm_model_name_edit_btn").click(function () {
        const data = {
            "name": $("#edit_model_name_input").val(),
            "model_name": activeModelId,
        };
        $.ajax({
            url: "/contentModel",
            data: JSON.stringify(data),
            error: function(data) {
                $("#edit_model_name_modal").modal("toggle")
                $("#edit_model_name_modal_result").modal("toggle")

                if (data.status === 400) {
                    $("#edit_model_name_modal_result_text").text("Виникла помилка.")
                    return
                }

                $("#edit_model_name_modal_result_text").text(data.responseJSON.error)
            },
            dataType: "json",
            success: function(data) {
                $("#edit_model_name_modal").modal("toggle")
                $("#edit_model_name_modal_result").modal("toggle")
                $("#edit_model_name_modal_result_text").text("Ім'я моделі було успішно оновлено.")
            },
            type: "PATCH"
        });
    })

    $("#confirm_model_remove_btn").click(function () {
        $.ajax({
            url: "/contentModel/" + activeModelId,
            error: function(data) {
                $("#delete_model_modal_result").modal("toggle")

                if (data.status === 400) {
                    $("#delete_model_result_text").text("Виникла помилка.")
                    return
                }

                $("#delete_model_result_text").text(data.error)
            },
            success: function(data) {
                $("#delete_model_modal_result").modal("toggle")
                $("#delete_model_result_text").text("Модель було успішно видалено.")
            },
            type: "DELETE"
        });
    })

    $("#confirm_model_create_btn").click(function () {
        const data = {
            "name": $("#create_model_name_input").val(),
        };
        $.ajax({
            data: JSON.stringify(data),
            url: "/contentModel",
            dataType: "json",
            error: function(data) {
                $("#create_model_modal_result").modal("toggle")
                $("#create_model_result_text").text(data.responseJSON.error)
            },
            success: function(data) {
                $("#create_model_modal_result").modal("toggle")
                $("#create_model_result_text").text("Нову модель було успішно створено.")
            },
            type: "POST"
        });
    })

    $("#edit_model_name_modal_result").on("hidden.bs.modal", function () {
        location.reload()
    })
    $("#delete_model_modal_result").on("hidden.bs.modal", function () {
        location.reload()
    })
    $("#create_model_modal_result").on("hidden.bs.modal", function () {
        location.reload()
    })
    $("#edit_attr_modal_result").on("hidden.bs.modal", function () {
        location.reload()
    })
    $("#create_attrib_modal_result").on("hidden.bs.modal", function () {
        location.reload()
    })
    $("#delete_attr_modal_result").on("hidden.bs.modal", function () {
        location.reload()
    })

    $("div")
        .filter(function () {
            return this.id.match(/.*_attr_name_.*/)
        })
        .mouseenter(onAttrNameEnter)

    $("#confirm_attr_delete_btn").click(function () {
        $("#edit_attr_modal").modal("toggle")
        $("#delete_attr_modal").modal("toggle")
    })

    $("#confirm_attr_remove_btn").click(function () {
        $("#delete_attr_modal").modal("toggle")

        const data = {
            "model_name": activeModelId,
            "attribute_id": parseInt(activeAttrId)
        };
        $.ajax({
            data: JSON.stringify(data),
            url: "/attribute",
            dataType: "json",
            error: function(data) {
                $("#delete_attr_modal_result").modal("toggle")
                $("#delete_attr_result_text").text(data.responseJSON.error)
            },
            success: function(data) {
                $("#delete_attr_modal_result").modal("toggle")
                $("#delete_attr_result_text").text("Атрибут було успішно видалено.")
            },
            type: "DELETE"
        });
    })

    $("#confirm_attr_create_btn").click(function () {
        $("#create_attrib_modal").modal("toggle")

        let attrTypeId = $("#types_select").val()

        const data = {
            "model_name": activeModelId,
            "type_id": parseInt(attrTypeId),
            "name": $("#create_attr_name_input").val()
        };
        $.ajax({
            data: JSON.stringify(data),
            url: "/attribute",
            dataType: "json",
            error: function(data) {
                $("#create_attrib_modal_result").modal("toggle")
                $("#create_attr_result_text").text(data.responseJSON.error)
            },
            success: function(data) {
                $("#create_attrib_modal_result").modal("toggle")
                $("#create_attr_result_text").text("Атрибут було успішно створено.")
            },
            type: "POST"
        });
    })
})

let activeModelId = undefined;
let activeAttrId = undefined;

function onAttrNameEnter() {
    const id = $(this).attr('id')
    const parts = id.split("_")
    activeModelId = parts[0]
    activeAttrId = parts[parts.length - 1]

    $(this).replaceWith('<button id="edit_attr" type="button" class="btn btn-outline-primary">' +
        '    <span class=\"fas fa-edit\"></span>' +
        '</button>'
    )

    $("#edit_attr").mouseleave(onAttrNameLeave)
    $("#edit_attr").click(onEditAttrClick)
}

function onEditAttrClick() {
    let attr = getAttr(activeModelId, parseInt(activeAttrId))

    $("#edit_attr_modal").modal('toggle')
    $("#edit_attr_name_input").val(attr.name)

    $("#confirm_attr_edit_btn").click(function () {
        const data = {
            "model_name": activeModelId,
            "attrib_id": parseInt(activeAttrId),
            "name": $("#edit_attr_name_input").val(),
        };
        $.ajax({
            data: JSON.stringify(data),
            url: "/attribute",
            dataType: "json",
            error: function(data) {
                $("#edit_attr_modal_result").modal("toggle")
                $("#edit_attributes_name_result_text").text(data.responseJSON.error)
            },
            success: function(data) {
                $("#edit_attr_modal_result").modal("toggle")
                $("#edit_attributes_name_result_text").text("Ім'я атрибуту було успішно оновлено.")
            },
            type: "PATCH"
        });
    })
}

function onAttrNameLeave() {
    let id = activeModelId + "_attr_name_" + activeAttrId
    let attr = getAttr(activeModelId, parseInt(activeAttrId))
    const attrName = attr.name
    $(this).replaceWith(`<div id="${id}">${attrName}</div>`)

    $("#" + id).mouseenter(onAttrNameEnter)
}

function getAttr(modelId, attrId) {
    const models = JSON.parse($.cookie("models"))
    let modelIdx = -1
    for (i = 0; i < models.length; ++i) {
        if (models[i].name === modelId) {
            modelIdx = i
            break
        }
    }

    for (i = 0; i < models[modelIdx].attributes.length; ++i) {
        if (models[modelIdx].attributes[i].id === attrId) {
            return models[modelIdx].attributes[i]
        }
    }
}

function onEditModelClick() {
    const id = $(this).attr('id')
    const parts = id.split("_")
    activeModelId = parts[parts.length - 1]

    $.get("/contentModel/" + activeModelId, function (data) {
        $("#edit_model_name_modal").modal('toggle')
        $("#edit_model_name_input").val(data.contentModel.name)
    })
}

function onRemoveModelClick() {
    const id = $(this).attr('id')
    const parts = id.split("_")
    activeModelId = parts[parts.length - 1]

    $("#delete_model_modal").modal("toggle")
}

function onAddAttribClick() {
    const id = $(this).attr('id')
    const parts = id.split("_")
    activeModelId = parts[parts.length - 1]

    $("#create_attrib_modal").modal("toggle")
}

function onCreateModelClick() {
    $("#create_model_modal").modal("toggle")
}

function setModelsData(data) {
    $.cookie("models", JSON.stringify(data))
}
